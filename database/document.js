const db = require('../config/database');


module.exports = {
    getFullDatail: async (id_process,type,active) =>{

	let result = await db.query(`
    FOR d IN 1 ANY @id_process hasProcessDocument FILTER d.active == @active
    FOR s IN 1 ANY d._id hasDocumentSubject
    FOR ec IN 1 ANY d._id hasEventPurpose FILTER ec._type==@type
    FOR ei IN 1 ANY ec._id hasEventSender FILTER ei._type == 'IndividualPerson' 
    FOR ea IN 1 ANY ec._id hasEventSender FILTER ea._type == 'Administrative'
    SORT ec.time DESC               
    return {document:d,subject:s,entity:ei,super:ea,event:ec}
    
`,{	id_process:id_process,
	type:type,active:active})

	return result._result;
}
}