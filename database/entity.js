const db = require('../config/database');


module.exports = {
	getListEntityLevelDown: async (id_entity)=>{
		const result = await db.query(`for e in 1..1 INBOUND @id_entity  hasEntitySuperEntity FILTER e.status!="deleted" return e`,
		{id_entity:id_entity});
	
		return result._result;
	}
}