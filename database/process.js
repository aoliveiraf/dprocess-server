const db = require('../config/database');


module.exports = {
	getProcess: async (id_process)=>{
		return await db.collection('process').byExample({_id:id_process})
	},
    getFullDatail: async (
	id_process="",
	id_super_super,
	id_super="",
	id_entity="",
	partReqName="",
	partIndName="",
	partAdmName="",
	partNameSubject="",
	currentActive=true,
	processActive=true) =>{

	let result = await db.query(`
	FOR super IN 1 INBOUND @id_super_super hasEntitySuperEntity 
    FILTER super._id == @id_super || @id_super==""
    
FOR p,h IN 1 ANY super._id hasEntityCurrentProcess 
    FILTER h.active==@currentActive || @currentActive == ""
	FILTER CONTAINS(LOWER(p.id), LOWER(@id_process)) || @id_process == ""
	FILTER p.active == @processActive || @processActive ==""

FOR req,hreq IN 1 INBOUND p._id hasEntityCurrentProcess FILTER hreq.requester==true 
    FILTER CONTAINS(LOWER(req.name), LOWER(@partReqName)) || @partReqName == ""
FOR ind1,hind IN 1 INBOUND p._id hasEntityCurrentProcess FILTER hind.requester==false
    FILTER ind1._type=='IndividualPerson'
    FILTER CONTAINS(LOWER(ind1.name), LOWER(@partIndName)) || @partIndName == ""
    FILTER ind1._id == @id_entity || @id_entity == ""
FOR adm1,hadm IN 1 INBOUND p._id hasEntityCurrentProcess FILTER hadm.requester==false
    FILTER adm1._type == 'Administrative'
    FILTER CONTAINS(LOWER(adm1.name), LOWER(@partAdmName)) || @partAdmName == ""

FOR s IN 1 ANY p._id hasProcessSubject 
    FILTER CONTAINS(LOWER(s.name), LOWER(@partNameSubject)) || @partNameSubject == ""


let listEventReceiver = (FOR ev IN 1 ANY p._id  hasEventPurpose SORT ev.time
				 FOR rind IN 1 ANY ev._id hasEventReceiver FILTER rind._type=='IndividualPerson'  
				 FOR radm IN 1 ANY ev._id hasEventReceiver FILTER radm._type=='Administrative'

				 return {event:ev,individual:rind,administrative:radm}) 
let eventPromise = (FOR ev IN 1 ANY p._id  hasEventPurpose FILTER ev.active==true and ev._type=='PromiseConnection'
					FOR adm IN 1 ANY ev._id hasEventReceiver FILTER adm._type=='Administrative'

					return {event:ev,administrative:adm}
)

let allDocumentUp = COUNT(
	FOR dd IN 1 ANY p._id hasProcessDocument 
	FILTER dd.active == true and (HAS(dd,'path') == false and dd.text=='')  return dd
) == 0

let listDetailDocument = (
	FOR d IN 1 ANY p._id hasProcessDocument FILTER d.active == true
	FOR ss IN 1 ANY d._id hasDocumentSubject
	FOR ec IN 1 ANY d._id hasEventPurpose FILTER ec._type=='Creation'
	FOR ei IN 1 ANY ec._id hasEventSender FILTER ei._type == 'IndividualPerson' 
	FOR ea IN 1 ANY ec._id hasEventSender FILTER ea._type == 'Administrative'
	SORT ec.time DESC               
	return {document:d,subject:ss,entity:ei,super:ea,event:ec}
	)                 

return {process:p,subjectProcess:s,concerned:req,listEventReceiver:listEventReceiver,eventPromise:eventPromise[0],listDetailDocument:listDetailDocument,allDocumentUp:allDocumentUp}

`,{	id_process:id_process,
	id_super_super:id_super_super,
	id_super:id_super,
	id_entity:id_entity,
	partReqName:partReqName,
	partIndName:partIndName,
	partAdmName:partAdmName,
	partNameSubject:partNameSubject,
	currentActive:currentActive,
	processActive:processActive
})

	return result._result;
}
}