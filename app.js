const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const helmet = require('helmet');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/user');
const entityRouter = require('./routes/entity');
const digitalProcessRouter = require('./routes/process');
const documentRouter = require('./routes/document');
const individualPersonInRouter = require('./routes/individualPersonIn');
const individualPersonOutRouter = require('./routes/individualPersonOut');
const openProcessEventRouter = require('./routes/openProcessEvent');
const admUnit = require('./routes/administrative');
const legalPerson = require('./routes/legalPerson');
const subject = require('./routes/subjectProcess');
const movement = require('./routes/movementProcess');
const searchProcess = require('./routes/searchProcess');
const upload = require('./routes/upload');
const address = require('./routes/address');
const subjectDocument = require('./routes/subjectDocument');
const administrativeSubjectProcess = require('./routes/administrativeSubjectProcess')
const jwt = require('jsonwebtoken');


verifyToken = function (req,res,next) {
}

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(helmet());
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*
app.use(function (req, res, next) {
  
  if (req.originalUrl === '/user/login') {

    return next();
  } else {

    if(!req.headers.authorization) {

      return res.status(401).send('Unauthorized request.')
    }
    let token = req.headers.authorization.split(' ')[1]
  
    if(token === 'null') {
      return res.status(401).send('Unauthorized request.')
    }
    
    let payload = jwt.verify(token,'m1ss2r34')
  
    if(!payload) {
  
      return res.status(401).send('Unauthorized request')
    }
    
    req.userId = payload.subject

    next()  
  }
})
*/
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/entity',entityRouter);
app.use('/process',digitalProcessRouter);
app.use('/document',documentRouter);
app.use('/entity/individualPersonIn',individualPersonInRouter);
app.use('/entity/individualPersonOut',individualPersonOutRouter);
app.use('/openProcessEvent',openProcessEventRouter);
app.use('/entity/administrative',admUnit);
app.use('/entity/legalPerson',legalPerson);
app.use('/subjectProcess',subject)
app.use('/movementProcess',movement);
app.use('/searchProcess',searchProcess);
app.use('/upload',upload);
app.use('/address',address);
app.use('/subjectDocument',subjectDocument);
app.use('/administrativeSubjectProcess',administrativeSubjectProcess);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
