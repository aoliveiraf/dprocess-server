var express = require('express');
var router = express.Router();
var db = require('../config/database');
var Joi = require('joi');
var Correios = require('node-correios');


//const mainSchema = require('../models/Document');


router.use((req, res, next) => {
	next();
})

async function saveAddress(data,res) {
	let id_creator = data.id_creator;
	delete data.id_creator;

	data.cep = data.cep.replace('-',"");
//	if(listPartCep.length >0) {
//		data.cep = listPartCep[0] + listPartCep[1]
//	}
	
	db.query(`
		INSERT @data INTO address LET new_address = NEW

		INSERT {_type:@_type,timeStart:@time,timeEnd:@time, active:true} IN event let new_event = NEW
		INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
		INSERT {_from:new_event._id,_to:new_address._id} INTO hasEventPurpose

		RETURN new_address`,
		{ data: data, id_creator: id_creator, _type: 'Creation', time: Date.now() })
		.then((cursor) => {
			cursor.all().
			then((result) => {
				res.status(200).send(result[0]);
			}).catch((error) => {
				console.log(`Router - POST /address failed: ${error}`);
				res.status(500).send(`Router - POST /address failed: ${error}`);
			});
		}).catch((error) => {
			console.log(`Router - POST /address failed: ${error}`);
			res.status(500).send(`Router - POST /address failed: ${error}`);
		});
}

router.get('/:id', (req, res, next) => {
	let cep = req.params.id;
	let id_creator = `entity/${req.query.id_creator}`;

	db.collection('address').firstExample({ cep: cep })
		.then((result) => {
			res.status(200).send(result)
		}).catch((error) => {
			correios = new Correios();
			correios.consultaCEP({ cep: cep })
			.then(result => {
				result.id_creator=id_creator;
				if(!result.erro)
					saveAddress(result,res)
				else {
					console.log("CEP não localizado.");
					
					res.status(304).send('erro')
				}
			});
		});
});

router.post('/', (req, res, next) => {
	let body = req.body;
	
	body.id_creator = `entity/${body.id_creator}`;;
	body.cep=body.id;
	delete body.id;

	saveAddress(body,res)

});

module.exports = router;
