var express = require('express');
var router = express.Router();
const User = require('../models/User');
const db = require('../config/database');
const jwt = require('jsonwebtoken');
const Util = require('../models/Util')



router.post('/adm', (req, res, next) => {
  //  User.validate(req.body);
  let _user = req.body;
  let id_creator = `entity/${_user.key_creator}`;
  let id_entity = `entity/${_user.key_unit}`;
  delete _user.key_creator;
  delete _user.key_unit;
  _user.listType = ['Administrator']

  const password = _user.password;
  delete _user.password;

  let saltAndHash = Util.user.encryptPassword(password);

  _user.hash = saltAndHash.hash;
  _user.salt = saltAndHash.salt;

  db.query(
    `INSERT @user INTO user LET new_user = NEW
     INSERT {_type:'Creation',timeStart:@time,timeEnd:@time, active:true} INTO event let new_event = NEW
     INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
     INSERT {_from:new_event._id,_to:new_user._id} INTO hasEventPurpose
     INSERT {_from:new_user._id, _to:@id_entity} INTO hasUserAdministratorEntity
     INSERT {_from:@id_entity, _to:new_user._id} INTO hasEntityUser

     RETURN new_user`,
    { user: _user, id_creator: id_creator, id_entity: id_entity, time: Date.now() }
  )

    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router User - POST /user/adm failed: ${error}`);
          res.status(500).send(`Server Interval Error`);
        });
    })
    .catch((error) => {
      console.log(`Router User - POST /user/adm failed: ${error}`);
      res.status(500).send(`Server Interval Error`);
    });
})

router.get('/searchName', (req, res, next) => {
  let searchName = req.query.searchName;

  let query = `FOR u IN user FILTER u.name == @searchName RETURN u`;
  db.query(query, { searchName: searchName })
    .then((cursor) => {
      cursor.all()
        .then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router - GET /user?searchName failed: ${error}`);
          res.status(500).send(`Router - GET /user?searchName failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router - GET /user?searchName failed: ${error}`);
      res.status(500).send(`Router - GET /user?searchName failed: ${error}`);
    });
});


router.put('/adm/update', (req, res, next) => {

  let id_creator = `entity/${req.body.key_creator}`;
  let key_user = req.body.key_user;
  let id_administared = `entity/${req.body.key_administared}`;
  let listType = req.body.listType;
  let id_user = `user/${key_user}`;

  db.query(
    `
      UPDATE @key_user WITH {listType:@listType} IN user let new_user = NEW

      INSERT {_type:'Update',timeStart:@time, active:true} INTO event let new_event = NEW
      
      INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
      INSERT {_from:new_event._id,_to:@id_user} INTO hasEventPurpose
      
      INSERT {_from:@id_user, _to:@id_administared} INTO hasUserAdministratorEntity

       RETURN new_user`,
    { key_user: key_user, listType: listType, id_user: id_user, id_creator: id_creator, id_administared: id_administared,time: Date.now() }
  )
    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router PUT /user/adm/update failed: ${error}`);
          res.status(500).send(`Router PUT /user/adm/update failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router PUT /user/adm/update failed: ${error}`);
      res.status(500).send(`Router PUT /user/adm/update failed: ${error}`);
    });
})
/*
router.put('/adm/update', (req, res, next) => {
  //  User.validate(req.body);

  let id_creator = `entity/${req.body.key_creator}`;
  let key_user = req.body.key_user;
  let id_administared = `entity/${req.body.key_administared}`;
  let password = req.body.password;
  let id_entity = `entity/${req.body.key_entity}`;
  let listType = req.body.listType;
  let id_user = `user/${key_user}`;
  const saltAndHash = Util.user.encryptPassword(password);
  let salt = saltAndHash.salt;
  let hash = saltAndHash.hash;

  db.query(
    `
      UPDATE @key_user WITH {salt:@salt,hash:@hash,listType:@listType} IN user let new_user = NEW

      INSERT {_type:'Update',timeStart:@time, active:true} INTO event let new_event = NEW
      
      INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
      INSERT {_from:new_event._id,_to:@id_user} INTO hasEventPurpose
      
//      INSERT {_from:@id_entity, _to:@id_user} INTO hasEntityUser
      
      INSERT {_from:@id_user, _to:@id_administared} INTO hasUserAdministratorEntity

       RETURN new_user`,
    { key_user: key_user, listType: listType, id_user: id_user, id_creator: id_creator, id_administared: id_administared, salt:salt,hash:hash,time: Date.now() }
  )
    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router PUT /user/adm/update failed: ${error}`);
          res.status(500).send(`Router PUT /user/adm/update failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router PUT /user/adm/update failed: ${error}`);
      res.status(500).send(`Router PUT /user/adm/update failed: ${error}`);
    });
})
*/
router.post('/adm/new', (req, res, next) => {
  //  User.validate(req.body);
  let name = req.body.name;
  let id_creator = `entity/${req.body.key_creator}`;
  //  let key_user = req.body.key_user;
  let id_administared = `entity/${req.body.key_administared}`;
  let password = req.body.password;
  let id_entity = `entity/${req.body.key_entity}`;
  let listType = req.body.listType;
  const saltAndHash = Util.user.encryptPassword(password);
  let salt = saltAndHash.salt;
  let hash = saltAndHash.hash;

  db.query(
    `
      INSERT {name:@name,salt:@salt,hash:@hash,listType:@listType} IN user let new_user = NEW

      INSERT {_type:'Creation',timeStart:@time, active:true} INTO event let new_event = NEW
      
      INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
      INSERT {_from:new_event._id,_to:new_user._id} INTO hasEventPurpose
      
      INSERT {_from:@id_entity, _to:new_user._id} INTO hasEntityUser
      
      INSERT {_from:new_user._id, _to:@id_administared} INTO hasUserAdministratorEntity

       RETURN new_user`,
    { name: name, listType: listType, id_creator: id_creator, id_administared: id_administared, salt: salt, hash: hash, id_entity: id_entity, time: Date.now() }
  )
    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router POST /user/adm/new failed: ${error}`);
          res.status(500).send(`Router POST /user/adm/new failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router POST /user/adm/new failed: ${error}`);
      res.status(500).send(`Router POST /user/adm/new failed: ${error}`);
    });
})

router.post('/', async (req, res, next) => {
  let name = req.body.name;
  let id_creator = `entity/${req.body.key_creator}`;
  let id_entity = `entity/${req.body.key_entity}`;
  let listType = req.body.listType;

  const result = await db.query(`for e in entity filter e._id == @id_entity return e`,{id_entity:id_entity});


  const entity = result._result[0];

  const saltAndHash = Util.user.encryptPassword(entity.id);
  let salt = saltAndHash.salt;
  let hash = saltAndHash.hash;

  db.query(
    `
      INSERT {name:@name,salt:@salt,hash:@hash,listType:@listType} IN user let new_user = NEW

      INSERT {_type:'Creation',timeStart:@time, active:true} INTO event let new_event = NEW
      
      INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
      INSERT {_from:new_event._id,_to:new_user._id} INTO hasEventPurpose
      
      INSERT {_from:@id_entity, _to:new_user._id} INTO hasEntityUser
      
       RETURN new_user`,
    { name: name, listType: listType, id_creator: id_creator, salt: salt, hash: hash, id_entity: id_entity, time: Date.now() }
  )
    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router POST /user failed: ${error}`);
          res.status(500).send(`Router POST /user failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router POST /user failed: ${error}`);
      res.status(500).send(`Router POST /user failed: ${error}`);
    });
})


router.put('/', (req, res, next) => {
  //  User.validate(req.body);

  let id_creator = `entity/${req.body.key_creator}`;
  let key_user = req.body.key_user;
  //  let id_administared = `entity/${req.body.key_administared}`;
  let listType = req.body.listType;
  let id_user = `user/${key_user}`;


  db.query(
    `
      UPDATE @key_user WITH {listType:@listType} IN user let new_user = NEW

      INSERT {_type:'Update',timeStart:@time, active:true} INTO event let new_event = NEW
      
      INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
      INSERT {_from:new_event._id,_to:@id_user} INTO hasEventPurpose
      
//      INSERT {_from:@id_entity, _to:@id_user} INTO hasEntityUser
      
 //     INSERT {_from:@id_user, _to:@id_administared} INTO hasUserAdministratorEntity

       RETURN new_user`,
    { key_user: key_user, listType: listType, id_user: id_user, id_creator: id_creator, time: Date.now() }
  )
    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router PUT /user failed: ${error}`);
          res.status(500).send(`Router PUT /user failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router PUT /user failed: ${error}`);
      res.status(500).send(`Router PUT /user failed: ${error}`);
    });
})

router.delete('/adm/:key_user', (req, res, next) => {
  //  User.validate(req.body);

  let id_user = `user/${req.params.key_user}`;

  db.query(
    `FOR en,h IN 1 ANY @id_user hasUserAdministratorEntity
      REMOVE h._key IN  hasUserAdministratorEntity let removed = OLD

       RETURN removed`,
    { id_user: id_user }
  )
    .then((cursor) => {
      cursor.all().then((result) => res.status(200).send(result))
        .catch((error) => {
          console.log(`Router DELETE /user/adm failed: ${error}`);
          res.status(500).send(`Router DELETE /user/adm failed: ${error}`);
        });
    })
    .catch((error) => {
      console.log(`Router PUT /user/adm failed: ${error}`);
      res.status(500).send(`Router PUT /user/adm failed: ${error}`);
    });
})


router.post('/login', async (req, res, next) => {
  let userData = req.body;
  try {
    const user = await db.collection("user").firstExample({ name: userData.name });
    const  valid = Util.user.validPassword(userData.password, user);
      if (valid) {
        db.query(`FOR e IN 1..2 INBOUND @id_user hasEntityUser, OUTBOUND hasEntitySuperEntity return e`, { id_user: user._id })
          .then((cursor) => {
            cursor.all().then((result) => {
              let payload = { subject: user._id };
              let token = jwt.sign(payload, 'm1ss2r34');

              res.status(200).send({ token: token, entity: result[0], super: result[1], user: { listType: user.listType, _id: user._id, name: user.name, _key: user._key } })
            })
          })
      } else {
        console.log(`Usuário ou senha inválidos`);
        res.status(500).send(`Usuário ou senha inválidos`);
    }
  } catch (error) {
    res.status(500).send(`Usuário ou senha inválidos`);
  }


})


router.get('/:key/entity/administered', (req, res, next) => {
  let id_user = `user/${req.params.key}`;

  db.query(`FOR e IN 1 ANY @id_user hasUserAdministratorEntity return e`, { id_user: id_user })
    .then((cursor) => {
      cursor.all().then((result) => {

        res.status(200).send(result);
      })
    })
    .catch((error) => {
      console.log(`Router User login - POST /${id_user}/entity/administrator failed: ${error}`);
      res.status(500).send(`Router User login - POST /${id_user}/entity/administrator failed: ${error}`);
    });
});

router.put('/password/change', async (req, res) => {
  const id_user = `user/${req.body.key_user}`;
  const password = req.body.password;
  let id_creator = `entity/${req.body.key_creator}`;

  await changePassword(id_user,id_creator,password,res);
})

router.put('/password/reset', async (req, res) => {
  const id_user = `user/${req.body.key_user}`;
  let id_creator = `entity/${req.body.key_creator}`;
  
  const result = await db.query(`for e in 1 any @id_user hasEntityUser return e`,{id_user:id_user}) 

  let entity = result._result[0];

  await changePassword(id_user,id_creator,entity.id,res);
})


async function changePassword(id_user,id_creator,password,res) {

  const saltAndHash = Util.user.encryptPassword(password);
  let salt = saltAndHash.salt;
  let hash = saltAndHash.hash;

  db.query(`
  for u in user filter u._id == @id_user
  update u with {salt:@salt,hash:@hash} in user let new_user = NEW 
  
  INSERT {_type:'Update',time:@time, active:true} INTO event let new_event = NEW      
  INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
  INSERT {_from:new_event._id,_to:@id_user} INTO hasEventPurpose

  return new_user`, { salt: salt, hash: hash,id_creator:id_creator,id_user:id_user, time: Date.now()})
    .then((cursor) => {
      cursor.all().then((result) => {
        let user = result.pop();
        res.status(200).send(user)

      })
    }).catch((error) => {

      res.status(500).send(`Router PUT /password/change failed: ${error}`);
    });

}

async function encryptAllPassword() {

  const result = await db.query(`for u in user return u`)

  const listUser = result._result;

  for (let i = 0; i < listUser.length; i++) {
    let user = listUser[i];
    const password = user.password;
    const saltAndHash = Util.user.encryptPassword(password);
    let salt = saltAndHash.salt;
    let hash = saltAndHash.hash;

    await db.query(`UPDATE @user WITH { password: null,hash:@hash,salt:@salt } IN user OPTIONS { keepNull: false }`, { user: user, salt: salt, hash: hash })
  }
}

//encryptAllPassword()

module.exports = router;
