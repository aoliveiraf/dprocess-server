const express = require('express');
const router = express.Router();
const db = require('../config/database');
const scheme = require('../models/SubjectProcess')

router.post('/', (req, res, next) => {

	let data = req.body;
	let id_super = `entity/${req.body.key_super}`;
	delete data.key_super;
	delete data._key;


	db.query(
		`FOR es IN 1 OUTBOUND @id_super hasEntitySuperEntity 

			INSERT {name:@name} INTO subjectProcess LET new_subject = NEW
		  	INSERT {_from:es._id,_to:new_subject._id} IN hasEntitySubjectProcess
	   RETURN new_subject`,
		{name:data.name,id_super:id_super}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result[0]))
			.catch((error) => {
					console.log(`Router - POST /subjectProcess failed: ${error}`);
					res.status(500).send(`Router - POST /subjectProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - POST /subjectProcess failed: ${error}`);
			res.status(500).send(`Router - POST /subjectProcess failed: ${error}`);
		});
})

router.put('/', (req, res, next) => {

	let data = req.body;

	db.query(
		`UPDATE @data._key WITH {name:@data.name} IN subjectProcess let new_subject = NEW
		   RETURN new_subject`,
		{data:data}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result[0]))
			.catch((error) => {
					console.log(`Router - PUT /subjectProcess failed: ${error}`);
					res.status(500).send(`Router - PUT /subjectProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - PUT /subjectProcess failed: ${error}`);
			res.status(500).send(`Router - PUT /subjectProcess failed: ${error}`);
		});
})

router.delete('/:key_subjectProcess/subjectDocument/:key_subjectDocument', (req, res, next) => {

	let key_subjectProcess = `subjectProcess/${req.params.key_subjectProcess}`;
	let key_subjectDocument = `subjectDocument/${req.params.key_subjectDocument}`;

	db.query(
		`
		FOR sd,h IN 1 ANY @key_subjectProcess hasSubjectProcessSubjectDocument FILTER sd._id == @key_subjectDocument
			REMOVE h._key IN hasSubjectProcessSubjectDocument let new_subject = OLD
		   RETURN new_subject`,
		{key_subjectDocument:key_subjectDocument,key_subjectProcess:key_subjectProcess}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - DELETE /key_subjectProcess/subjectDocument/:key_subjectDocument failed: ${error}`);
					res.status(500).send(`Router - DELETE /key_subjectProcess/subjectDocument/:key_subjectDocument failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - DELETE /key_subjectProcess/subjectDocument/:key_subjectDocument failed: ${error}`);
			res.status(500).send(`Router - DELETE /key_subjectProcess/subjectDocument/:key_subjectDocument failed: ${error}`);
		});
})


router.delete('/:key', (req, res, next) => {

	let key = req.params.key;

	db.query(
		`UPDATE @key WITH {status:'deleted'} IN subjectProcess let new_subject = NEW
		RETURN new_subject`,
		{key:key}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - DELETE /subjectProcess failed: ${error}`);
					res.status(500).send(`Router - DELETE /subjectProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - DELETE /subjectProcess failed: ${error}`);
			res.status(500).send(`Router - DELETE /subjectProcess failed: ${error}`);
		});
})

router.get('/entity/:key', (req, res, next) => {

	let searchName = req.query.searchName;
	let id_entity = `entity/${req.params.key}`;

	let query = `FOR es IN 1 ANY @id_entity hasEntitySuperEntity
				FOR s IN 1 ANY es._id hasEntitySubjectProcess 
				FILTER HAS(s,'status')==false and 
				(CONTAINS(LOWER(s.name), LOWER(@searchName)) 
				||@searchName == '') 
				SORT s.name 
				RETURN s`;
	db.query(query,{id_entity:id_entity,searchName:searchName})
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router entity/subject - GET /subject failed: ${error}`);
					res.status(500).send(`Router /subject - GET /subject failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router entity/subject - GET /subject failed: ${error}`);
			res.status(500).send(`Router /subject - GET /subject failed: ${error}`);
		});
});

router.post('/hasSubjectDocument', (req, res, next) => {

	let id_subjectProcess = `subjectProcess/${req.body.id_subjectProcess}`;
	let id_subjectDocument = `subjectDocument/${req.body.id_subjectDocument}`;


	db.query(
		`INSERT {_from:@id_subjectProcess,_to:@id_subjectDocument} INTO hasSubjectProcessSubjectDocument LET new_has = NEW
	   RETURN new_has`,
		{id_subjectProcess:id_subjectProcess,id_subjectDocument:id_subjectDocument}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - POST /subjectProcess/hasSubjectDocument failed: ${error}`);
					res.status(500).send(`Router - POST /subjectProcess/hasSubjectDocument failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - POST /subjectProcess/hasSubjectDocument failed: ${error}`);
			res.status(500).send(`Router - POST /subjectProcess/hasSubjectDocument failed: ${error}`);
		});
})

router.post('/canEntityConnectSubjectProcess', (req, res, next) => {

	let id_entity = `entity/${req.body.id_entity}`;
	let id_subjectProcess = `subjectProcess/${req.body.id_subjectProcess}`;
	let _listTypeDocumentCreation = req.body._listTypeDocumentCreation;
	let canCreateProcess=req.body.canCreateProcess;

	
	delete req.body.id_entity;
	delete req.body.id_subjectProcess;
	db.query(
		`INSERT {_from:@id_entity,_to:@id_subjectProcess,
			_listTypeDocumentCreation:@_listTypeDocumentCreation,
			canCreateProcess:@canCreateProcess} INTO canEntityConnectSubjectProcess LET new_has = NEW
	   RETURN new_has`,
		{id_subjectProcess:id_subjectProcess,
		id_entity:id_entity,
		_listTypeDocumentCreation:_listTypeDocumentCreation,
		canCreateProcess:canCreateProcess
	}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
					res.status(500).send(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
			res.status(500).send(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
		});
})

router.put('/canEntityConnectSubjectProcess', (req, res, next) => {

	let key_can = req.body.key_can;
	let _listTypeDocumentCreation = req.body._listTypeDocumentCreation;
//	let suggestionText = req.body.suggestionText;
	let canCreateProcess=req.body.canCreateProcess;
	db.query(
		`UPDATE @key_can WITH {_listTypeDocumentCreation:@_listTypeDocumentCreation,
			canCreateProcess:@canCreateProcess} INTO canEntityConnectSubjectProcess LET new_can = NEW
	   RETURN new_can`,
		{key_can:key_can,
		_listTypeDocumentCreation:_listTypeDocumentCreation,
		canCreateProcess:canCreateProcess
	}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - PUT /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
					res.status(500).send(`Router - PUT /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - PUT /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
			res.status(500).send(`Router - PUT /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
		});
})


router.get('/:id/listCanEntityConnect', (req, res, next) => {
	let id_subjectProcess = `subjectProcess/${req.params.id}`;
	
	let query = `FOR en,h IN 1 ANY @id_subjectProcess canEntityConnectSubjectProcess SORT en.name 
	RETURN {entity:en,has:h}`;
	db.query(query,{id_subjectProcess:id_subjectProcess})
		.then((cursor) => {
			cursor.all()
				.then((result) => { 
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router - GET /:id/listCanEntityConnect failed: ${error}`);
					res.status(500).send(`Router - GET /:id/listCanEntityConnect failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /:id/listCanEntityConnect failed: ${error}`);
			res.status(500).send(`Router - GET /:id/listCanEntityConnect failed: ${error}`);
		});
});

router.get('/:id/listSubjectDocument', (req, res, next) => {
	let id_subjectProcess = `subjectProcess/${req.params.id}`;
	
	let query = `FOR sd IN 1 ANY @id_subjectProcess hasSubjectProcessSubjectDocument SORT sd.name RETURN sd`;
	db.query(query,{id_subjectProcess:id_subjectProcess})
		.then((cursor) => {
			cursor.all()
				.then((result) => { 
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router - GET /:id/listSubjectDocument failed: ${error}`);
					res.status(500).send(`Router - GET /:id/listSubjectDocument failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /:id/listSubjectDocument failed: ${error}`);
			res.status(500).send(`Router - GET /:id/listSubjectDocument failed: ${error}`);
		});
});

/*
router.get('/', (req, res, next) => {
	let searchName = req.query.searchName;

	let query = `FOR s IN subjectProcess filter CONTAINS(s.name, @searchName) RETURN s`;
	db.query(query, { searchName: searchName })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router entity/subject - GET /subject failed: ${error}`);
					res.status(500).send(`Router /subject - GET /subject failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router entity/subject - GET /subject failed: ${error}`);
			res.status(500).send(`Router /subject - GET /subject failed: ${error}`);
		});
});
*/
router.get('/:id/canConnect/entity', (req, res, next) => {
	let id_administrative = `entity/${req.query.idSuper}`;
	let id_subject = `subjectProcess/${req.params.id}`;

	let query = `FOR e IN 1 ANY @id_subject canEntityConnectSubjectProcess filter e._id != @id_administrative RETURN e`;
	db.query(query, { id_administrative: id_administrative, id_subject: id_subject })
		.then((cursor) => {
			cursor.all()
				.then((result) => { res.status(200).send(result); })
				.catch((error) => {
					console.log(`Router GET /:id/canConnect/entity failed: ${error}`);
					res.status(500).send(`Router - GET /:id/canConnect/entity failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /:id/canConnect/entity failed: ${error}`);
			res.status(500).send(`Router - GET /:id/canConnect/entity failed: ${error}`);
		});
});

module.exports = router;
