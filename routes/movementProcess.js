var express = require('express');
var router = express.Router();
var db = require('../config/database');
//var filter = require('./filter');
var joi = require('joi');

const schema = joi.object().keys({
	listIdProcess: joi.array().items(joi.string()).required(),
	id_creator: joi.string().required(),
	id_entityTo: joi.string().required()
});

router.use((req, res, next) => {

	next();
})


router.post('/get', (req, res) => {
	let body = req.body;
	let id_creator = `entity/${body.id_entityTo}`;
	let id_entityFrom=`entity/${body.id_entityFrom}`;
	let id_super = `entity/${body.id_super}`;
	let listIdProcess = body.listIdProcess;

	listIdProcess.forEach((idProcess, i) => {
		element = `process/${idProcess}`;
		listIdProcess[i] = element;
	});
	console.log(listIdProcess);
	db.query(`
		let listData = [@id_creator,@id_super]
	
		FOR id_p IN @listIdProcess
		FOR en,h IN 1 ANY id_p hasEntityCurrentProcess FILTER en._id ==@id_entityFrom and h.active == true
			INSERT {_type:@_type,time:@time, active:true} IN event let new_event = NEW
			INSERT {_from:new_event._id,_to:id_p} INTO hasEventPurpose
			UPDATE h WITH {_from:@id_creator,active:true} IN hasEntityCurrentProcess

		FOR d IN listData
			INSERT {_from:new_event._id,_to:d} INTO hasEventSender
			INSERT {_from:new_event._id,_to:d} INTO hasEventReceiver


			RETURN new_event	
				`,
		{
			listIdProcess: listIdProcess,
			id_super: id_super,
			_type: 'Connection',
			id_entityFrom:id_entityFrom,
			id_creator: id_creator,
			time: Date.now()
		})
		.then((cursor) => {
			cursor.all().then((result) => {
				res.status(200).send(result);
			})
				.catch((error) => {
					console.log(`Router - POST /getProcess failed: ${error}`);
					res.status(500).send(`Router - POST /getProcess failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - POST /getProcess failed: ${error}`);
			res.status(500).send(`Router - POST /getProcess failed: ${error}`);
		});
});


router.post('/send', (req, res, next) => {
	let body = req.body;
	let id_entityTo = `entity/${body.id_entityTo}`;
	let id_creator = `entity/${body.id_entityFrom}`;
	let id_super = `entity/${body.id_super}`;
	let listIdProcess = body.listIdProcess;

	listIdProcess.forEach((idProcess, i) => {
		element = `process/${idProcess}`;
		listIdProcess[i] = element;
	});

	console.log('AQUI!!');
	
	db.query(`
			let listData = [@id_creator,@id_super]
	
			FOR id_p IN @listIdProcess
				INSERT {_type:@_type,time:@time, active:true} IN event let new_event = NEW
				INSERT {_from:new_event._id,_to:id_p} INTO hasEventPurpose
				INSERT {_from:new_event._id,_to:@id_entityTo} INTO hasEventReceiver
					
				FOR d IN listData
					INSERT {_from:new_event._id,_to:d} INTO hasEventSender
	
				RETURN new_event`,
		{
			listIdProcess: listIdProcess,
			id_super: id_super,
			_type: 'PromiseConnection',
			id_creator: id_creator,
			id_entityTo: id_entityTo,
			time: Date.now()
		})
		.then((cursor) => {
			cursor.all().then((result) => {
				updateHasCurrentProcess(req, res);
				res.status(200).send(result);

			})
				.catch((error) => {
					console.log(`Router - POST /movementProcess/send failed: ${error}`);
					res.status(500).send(`Router - POST /movementProcess/send failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - POST /movementProcess/send failed: ${error}`);
			res.status(500).send(`Router - POST /movementProcess/send failed: ${error}`);
		});
});

async function updateHasCurrentProcess(req, res) {

	let body = req.body;
	let id_creator = `entity/${body.id_entityFrom}`;
	let id_super = `entity/${body.id_super}`;
	let listIdProcess = body.listIdProcess;

	db.query(`
		let listData = [@id_creator,@id_super]
	
		FOR id_p IN @listIdProcess				
			FOR d IN listData
				FOR e,h IN 1 ANY id_p hasEntityCurrentProcess FILTER h.active == true and e._id == d  
					UPDATE h._key WITH {active:false} IN hasEntityCurrentProcess 
	
			RETURN h`,
		{
			listIdProcess: listIdProcess,
			id_super: id_super,
			id_creator: id_creator,
		})
		.then((cursor) => {
			console.log('updateHasCurrentProcess', cursor.all());

		}).catch((error) => {
			console.log(`Router - POST /movementProcess/send failed: ${error}`);
		});
}

router.post('/receive', (req, res, next) => {
	let body = req.body;
	let id_creator = `entity/${body.idEntity}`;
	let id_super = `entity/${body.idSuper}`;
	let id_event = `event/${body.idEvent}`;
	let id_process = `process/${body.idProcess}`;
	let key_event = body.idEvent;

	db.query(`
		LET hp =(FOR en,h IN 1 ANY @id_process hasEntityCurrentProcess FILTER h.active == false and h.requester==false sort en._type return h)

		LET listData = [@id_super,@id_creator]
		INSERT {_type:@_type,time:@time,active:true} INTO event let new_event = NEW
		INSERT {_from:new_event._id,_to:@id_process} INTO hasEventPurpose
		INSERT {_from:new_event._id,_to:@id_event} INTO hasEventPrevious

		FOR i IN 0..1
			INSERT {_from:new_event._id,_to:listData[i]} INTO hasEventReceiver
			INSERT {_from:new_event._id,_to:listData[i]} INTO hasEventSender
			UPDATE hp[i]._key WITH {_from:listData[i],active:true} IN hasEntityCurrentProcess

			RETURN new_event`,
		{
			id_process: id_process,
			_type: 'Connection',
			id_event: id_event,
			id_creator: id_creator,
			id_super: id_super,
			time: Date.now()
		})
		.then((cursor) => {
			cursor.all().then((result) => {
				updateEvent(key_event,{active:false})
				res.status(200).send(result)
			})
				.catch((error) => {
					console.log(`Router - POST /moventProcess/receive failed: ${error}`);
					res.status(500).send(`Router - POST /moventProcess/receive failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - POST /moventProcess/receive failed: ${error}`);
			res.status(500).send(`Router - POST /moventProcess/receive failed: ${error}`);
		});
	//	}

});

async function updateOneHasCurrentProcess(req, res) {

	let body = req.body;
	let id_event = `event/${body.idEvent}`;

	db.query(`		
		FOR ev IN 1 ANY @id_event hasEventPrevious FILTER ev._type == "Rejection" 
    		FOR p IN 1 OUTBOUND @id_event hasEventPurpose
        		FOR etts IN 1 OUTBOUND @id_event hasEventSender 
            		FOR ettc,h IN 1 INBOUND p._id hasEntityCurrentProcess FILTER  ettc._id == etts._id and h.active == false
                		UPDATE h._key WITH {active:true} IN hasEntityCurrentProcess let new = NEW
		return new`,
		{
			id_event: id_event
		})
		.then((cursor) => {
			cursor.all().then((result) => console.log('Seccess',result))
				.catch((error) => console.log(`Router - POST /moventProcess/reject failed: ${error}`));
		})
		.catch((error) => console.log(`Router - POST /moventProcess/reject failed: ${error}`));

}
async function updateEvent(key_event, data) {

	db.query(`		
		UPDATE @key_event WITH @data IN event let new = NEW
		return new`,
		{
			key_event: key_event,
			data:data
		})
		.then((cursor) => {
			cursor.all().then((result) => console.log('Seccess',result))
				.catch((error) => console.log(`Router - POST /moventProcess/reject failed: ${error}`));
		})
		.catch((error) => console.log(`Router - POST /moventProcess/reject failed: ${error}`));

}

router.post('/reject', (req, res, next) => {
	let body = req.body;
	let id_creator = `entity/${body.idEntity}`;
	let id_super=`entity/${body.idSuper}`;
	let key_event = body.idEvent;
	let id_event = `event/${body.idEvent}`;
	let id_process = `process/${body.idProcess}`;

	db.query(`			
			INSERT {_type:@_type,time:@time,active:true} INTO event let new_event = NEW

			INSERT {_from:new_event._id,_to:@id_process} INTO hasEventPurpose
			INSERT {_from:new_event._id,_to:@id_event} INTO hasEventPrevious
			let listSender = [@id_super,@id_creator]
			FOR d IN listSender
				INSERT {_from:new_event._id,_to:d} INTO hasEventSender

			RETURN new_event`,
		{
			id_process: id_process,
			_type: 'Rejection',
			id_event: id_event,
			id_creator: id_creator,
			id_super:id_super,
			time: Date.now()
		})
		.then((cursor) => {
			cursor.all().then((result) => {
				updateOneHasCurrentProcess(req,res)
				updateEvent(key_event, {active:false})
				res.status(200).send(result)
			})
				.catch((error) => {
					console.log(`Router - POST /moventProcess/reject failed: ${error}`);
					res.status(500).send(`Router - POST /moventProcess/reject failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - POST /moventProcess/reject failed: ${error}`);
			res.status(500).send(`Router - POST /moventProcess/reject failed: ${error}`);
		});
});

module.exports = router;
