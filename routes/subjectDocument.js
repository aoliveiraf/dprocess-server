const express = require('express');
const router = express.Router();
const db = require('../config/database');
const scheme = require('../models/SubjectProcess')

router.post('/', (req, res, next) => {

	let data = req.body;
	data._type = 'Ordinary';

	db.query(
		`INSERT @data INTO subjectDocument LET new_subject = NEW
	   RETURN new_subject`,
		{data:data}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - POST /subjectDocument failed: ${error}`);
					res.status(500).send(`Router - POST /subjectDocument failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - POST /subjectDocument failed: ${error}`);
			res.status(500).send(`Router - POST /subjectDocument failed: ${error}`);
		});
})

router.put('/', (req, res, next) => {

	let data = req.body;

	db.query(
		`UPDATE @data._key WITH {name:@data.name} IN subjectDocument let new_subject = NEW
		   RETURN new_subject`,
		{data:data}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result[0]))
			.catch((error) => {
					console.log(`Router - PUT /subjectDocument failed: ${error}`);
					res.status(500).send(`Router - PUT /subjectDocument failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - PUT /subjectDocument failed: ${error}`);
			res.status(500).send(`Router - PUT /subjectDocument failed: ${error}`);
		});
})


router.get('/', (req, res, next) => {

	let searchName = req.query.searchName;
	
	let query = `FOR s IN subjectDocument SORT s.name FILTER s._type==@_type and (CONTAINS(LOWER(s.name),LOWER(@searchName)) or @searchName == '') RETURN s`;
	db.query(query,{_type:'Ordinary',searchName:searchName})
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET /subjectDocument failed: ${error}`);
					res.status(500).send(`Router - GET /subjectDocument failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /subjectDocument failed: ${error}`);
			res.status(500).send(`Router - GET /subjectDocument failed: ${error}`);
		});
});

router.get('/fit', (req, res, next) => {

	let searchName = req.query.searchName;
	
	let query = `FOR s IN subjectDocument SORT s.name FILTER s._type==@_type and (LOWER(s.name) == LOWER(@searchName) or @searchName == '') RETURN s`;
	db.query(query,{_type:'Ordinary',searchName:searchName})
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET /subjectDocument failed: ${error}`);
					res.status(500).send(`Router - GET /subjectDocument failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /subjectDocument failed: ${error}`);
			res.status(500).send(`Router - GET /subjectDocument failed: ${error}`);
		});
});

router.get('/:id_subjectProcess/listDifference', (req, res, next) => {
	let id_subjectProcess = `subjectProcess/${req.params.id_subjectProcess}`;
	let query = `
	let listSubjectDocument = (FOR sd IN 1 ANY @id_subjectProcess hasSubjectProcessSubjectDocument sort sd.name return sd)
	let listAllSubjectDocument = (FOR s IN subjectDocument SORT s.name RETURN s)
	
	return outersection(listSubjectDocument,listAllSubjectDocument)`;
	db.query(query,{id_subjectProcess:id_subjectProcess})
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result[0]);
				})
				.catch((error) => {
					console.log(`Router - GET /subjectDocument failed: ${error}`);
					res.status(500).send(`Router - GET /subjectDocument failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /subjectDocument failed: ${error}`);
			res.status(500).send(`Router - GET /subjectDocument failed: ${error}`);
		});
});


module.exports = router;
