var express = require('express');
var router = express.Router();
var db = require('../config/database');
//var filter = require('./filter');
var Joi = require('joi');

const mainSchema = Joi.object().keys({
	id_entity: Joi.string().required(),
    id_creator:Joi.string().required(),
    id_process:Joi.string().required()
}); 

router.use((req,res,next) => {
	next();
})

router.get('/process/:id', (req, res, next) => {
//	let filters = filter.catchParams('entity', 'e', req.params);
//	let query = `FOR e IN entity ${filters} RETURN e`;
    let id_process = req.params.id_process;
    let query = `FOR e IN document filter e.id_process = @id_process RETURN e`;
	db.query(query,{id_process:id})
		.then((cursor) => {
			cursor.all()
			.then((result) => res.status(200).send(result))
			.catch((error) => {
				console.log(`Router Document - GET /document failed: ${error}`);
				res.status(500).send(`Server Interval Error`);
			});
		})
		.catch((error) => {
			console.log(`Router Document - GET /document failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});
router.post('/', (req, res, next) => {
	let body = req.body;
	const validate = Joi.validate(body, mainSchema);
	if (validate.error) {
		res.status(400).send({ 'error': validate.error.toString(), 'model':{ id_creator: 'string', id_entity: 'string', id_process: 'string'}});
	} else {
        let id_creator = `entity/${body.id_creator}`;
        let id_process = `process/${body.id_process}`;
        let id_entity = `entity/${body.id_entity}`;
    
		db.query(`INSERT {id_creator:@id_creator,timeStart:@time,timeEnd:@time, active:true} INTO event let new_event = NEW
                  INSERT { _from:@id_process, _to:new_event._id} INTO hasOpenProcessEventProcess
                  INSERT { _from:@id_entity, _to:new_event._id} INTO hasOpenProcessEventEntity
				  RETURN new_event`, { id_creator:id_creator,id_process:id_process,id_entity:id_entity,time:Date.now()})
		.then((cursor) => {
			cursor.all().then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router openProcessEvent - POST /openProcessEvent failed: ${error}`);
					res.status(500).send(`Server Interval Error`);				
				});
			})
			.catch((error) => {
				console.log(`Router Document - POST /document failed: ${error}`);
				res.status(500).send(`Server Interval Error`);	
			});
	}
});

module.exports = router;
