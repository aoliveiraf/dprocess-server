const express = require('express');
const router = express.Router();
const db = require('../config/database');
const joi = require('joi');
const schema = require('../models/Process')
const utilFunction = require('../models/Util')
const process_func = require('../database/process')
const document_func = require('../database/document')

router.use((req,res,next) => {
	next();
})

router.get('/entity/:id/concern', (req, res, next) => {

	let id = `entity/${req.params.id}`;
	
	  let query = `
	  FOR p,h IN 1 ANY @id_entity hasEntityCurrentProcess FILTER h.active == true  
	  FOR s IN 1 ANY p._id hasProcessSubject    
	  return {process:p,subject:s}`;
	db.query(query,{id_entity:id})
		.then((cursor) => {
			cursor.all()
			.then((result) => { 
				res.status(200).send(result)
			})
			.catch((error) => {
				console.log(`Router - /entity/:id/concern failed: ${error}`);
				res.status(500).send(`Router - /entity/:id/concern failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - /entity/:id/concern failed: ${error}`);
			res.status(500).send(`Router - /entity/:id/concern failed: ${error}`);
		});
});

router.get('/:id/protocol',(req, res)=>{

	idProcess = req.params.id;

	db.query(`
	FOR p IN process FILTER p.id==@id
	FOR re,h IN 1 ANY p._id hasEntityCurrentProcess FILTER h.requester==true
	FOR s IN 1 ANY p._id hasProcessSubject  
	FOR ev IN 1 ANY p._id hasEventPurpose FILTER ev._type=='Creation'
	FOR es IN 1 ANY ev._id hasEventSender FILTER es._type=='Administrative'
	FOR er IN 1 ANY ev._id hasEventSender FILTER er._type=='IndividualPerson'

	return {concerned:re,subject:s,event:ev,super:es,creator:er,process:p}`,{id:idProcess})
	.then((cursor)=>{
		cursor.all()
		.then((result)=>{
			let date = new Date(result[0].event.time);
			let month=date.getMonth()+1;
			let time = date.getDate()+"/"+month+"/"+date.getFullYear() +" - " + date.getHours()+":"+date.getMinutes(); 
			utilFunction.sendEmail(
				{from:'noreplymossoro@gmail.com',pass:'N1r2ply3',to:result[0].concerned.email,
				process:{
					id:result[0].process.id,
					entity:result[0].super.name,
					concerned:result[0].concerned.name,
					subject:result[0].subject.name,
					time:time
				}})
 
			res.status(200).send(result[0])
		})
	})
	.catch((error) => {
		console.log(`Router - POST /protocol failed: ${error}`);
		res.status(500).send(`Server Interval Error: ${error}`);
	})
})

router.get('/:id',async (req,res)=>{

	let id_process = `process/${req.params.id}`

	result = await process_func.getProcess(id_process);

	res.send(result._result[0])
})

router.post('/', (req, res, next) => {
	let body = req.body;
//	body.id_concernedIn = body.id_creator;
//	body.id_administrative = body.id_creator;
	let data = new Date()
	
	idGenerate = new Date();
	body.id=idGenerate.getTime().toString();

	const validate = joi.validate(body, schema);
	if (validate.error) {
		res.status(400).send({ 'error': validate.error.toString(), 'model':joi.describe(schema)});
	} else {
		let id_creator = `entity/${body.id_creator}`;
	//	let id_concernedIn = `entity/${body.id_concernedIn}`;
		let id_administrative = `entity/${body.id_administrative}`;
		let id_concernedOut = `entity/${body.id_concernedOut}`;
		let id_subject = `subjectProcess/${body.id_subject}`;

		delete body.id_administrative;
		delete body.id_creator;
//		delete body.id_concernedIn;
		delete body.id_concernedOut;
		delete body.id_subject;

		db.query(`for c in config 
		update c._key with {idProcess:{counter:TO_NUMBER(c.idProcess.counter)+1}} in config return NEW.idProcess`)
		.then((cursor)=> {
			cursor.all()
			.then((result)=> {
				let idProcess = result.pop();
				let year = idProcess.year;
				let counter = idProcess.counter;
				let zeros = "000000".slice(counter.toString().length)
				body.id = year + zeros + counter;
				let vd = utilFunction.codeVerifier(body.id)
				body.id = year + zeros + counter + vd;
				body.active=true;
				db.query(`
	
				INSERT @body INTO process LET new_process = NEW
		
				INSERT {_from:new_process._id,_to:@id_subject} INTO hasProcessSubject

				INSERT {_type:'Creation',time:@time,active:true} INTO event let new_event = NEW
				
				let data = {
					sender:[@id_creator,@id_administrative],
					receiver:[
						{_from:new_event._id,_to:@id_creator},
						{_from:new_event._id,_to:@id_administrative}
					],
					current:[
						{_from:@id_creator,_to:new_process._id,active:true,requester:false},
						{_from:@id_administrative,_to:new_process._id,active:true,requester:false},
						{_from:@id_concernedOut,_to:new_process._id,active:true,requester:true}]
				}
				INSERT {_from:new_event._id,_to:new_process._id} INTO hasEventPurpose
				for i in 0..2 
					INSERT data.current[i] INTO hasEntityCurrentProcess
					FILTER i < 2
					INSERT data.receiver[i] INTO hasEventReceiver
					INSERT {_from:new_event._id,_to:data.sender[i]} INTO hasEventSender
			
				RETURN new_process`, 
				{body: body,
				 id_creator:id_creator,
				 id_concernedOut:id_concernedOut,
				 id_administrative:id_administrative, 
				 id_subject:id_subject,
				 time:Date.now()})
				.then((cursor) => {
					cursor.all().then((result) => {
						res.status(200).send(result[0])
					}).catch((error) => {
						console.log(`Router - POST /process failed: ${error}`);
						res.status(500).send(`Server Interval Error: ${error}`);				
					});
				})
				.catch((error) => {
					console.log(`Router - POST /process failed: ${error}`);
					res.status(500).send(`Router - POST /process failed: ${error}`);				
				});

			})
		})
		.catch((error) => {
			console.log(`Router - POST /process failed: ${error}`);
			res.status(500).send(`Router - POST /process failed: ${error}`);				
		});


	}
});

router.get('/:id/listSubject',(req,res)=>{

	let idProcess = `process/${req.params.id}`;

	db.query(`FOR ls IN 2..2 ANY @idProcess hasProcessSubject, hasSubjectProcessSubjectDocument   
			return ls`,{idProcess:idProcess})
	.then((cursor)=>{
		cursor.all()
		.then((result) => res.status(200).send(result))
		.catch((error) => {
			console.log(`Router - GET process/:id/listSubject failed: ${error}`);
			res.status(500).send(`Router - GET process/:id/listSubject failed: ${error}`);
		});
}).catch((error) => {
		console.log(`Router - GET process/:id/listSubject failed: ${error}`);
		res.status(500).send(`Router - GET process/:id/listSubject failed: ${error}`);				
	});
})



router.get('/:id/fullDetail',async(req,res)=>{

	let id_process = `process/${req.params.id}`;

	let list_id_super = await db.query(`
	FOR p IN process FILTER p._id == @id_process 
	FOR super,h IN 1 INBOUND @id_process hasEntityCurrentProcess FILTER h.requester==false
	FILTER super._type == 'Administrative'
	FOR super_super IN 1 OUTBOUND super._id hasEntitySuperEntity return {id_super:super._id,id_super_super:super_super._id,id_process:p.id} 
	`,{id_process:id_process});

	let result = await process_func.getFullDatail(list_id_super._result[0].id_process,
							list_id_super._result[0].id_super_super,
							list_id_super._result[0].id_super,
							"",
							"",
							"",
							"",
							""
							)
	res.send(result[0]);
})

router.get('/:id/detail',(req,res)=>{

	let idProcess = `process/${req.params.id}`;

	db.query(`FOR p IN process FILTER p._id==@idProcess 
				FOR s IN 1 ANY p._id hasProcessSubject    
			return {process:p,subject:s}`,{idProcess:idProcess})
	.then((cursor)=>{
		cursor.all()
		.then((result) => res.status(200).send(result[0]))
		.catch((error) => {
			console.log(`Router - GET process/:id/detail failed: ${error}`);
			res.status(500).send(`Router - GET process/:id/detail failed: ${error}`);
		});
}).catch((error) => {
		console.log(`Router - GET process/:id/detail failed: ${error}`);
		res.status(500).send(`Router - GET process/:id/detail failed: ${error}`);				
	});
})

router.get('/:id/listDocument',(req,res)=>{

	let idProcess = `process/${req.params.id}`;

	db.query(`FOR ld IN 1 ANY @idProcess hasProcessDocument FILTER ld.active == true  
			return ld`,{idProcess:idProcess})
	.then((cursor)=>{
		cursor.all()
		.then((result) => res.status(200).send(result))
		.catch((error) => {
			console.log(`Router - GET process/:id/listDocument failed: ${error}`);
			res.status(500).send(`Router - GET process/:id/listDocument failed: ${error}`);
		});
}).catch((error) => {
		console.log(`Router - GET process/:id/listDocument failed: ${error}`);
		res.status(500).send(`Router - GET process/:id/listDocument failed: ${error}`);				
	});
})

router.get('/:id/detail/listDocument/',async (req,res)=>{

	let id_process = `process/${req.params.id}`;

	let result = await document_func.getFullDatail(id_process,'Creation', true)

	res.send(result);
});

router.get('/:id/detail/last/document/',(req,res)=>{

	let idProcess = `process/${req.params.id}`;

	db.query(`
	let listEvent = 
	(FOR e IN 2 ANY @idProcess hasProcessDocument, hasEventPurpose FILTER e._type==@_type
	SORT e.timeStart DESC LIMIT 1 return e)
	FOR ee IN listEvent 
	    FOR dd IN 1 ANY ee._id hasEventPurpose
	        FOR s IN 1 ANY dd._id hasDocumentSubject
	            FOR en IN 2..2 OUTBOUND ee._id hasEventSender,hasEntitySuperEntity
	return {document:dd, subject:s,entity:en}
	`,{idProcess:idProcess,_type:"Creation"})
	.then((cursor)=>{
		cursor.all()
		.then((result) => {
			res.status(200).send(result)
		})
		.catch((error) => {
			console.log(`Router - GET process/:id/detail/listDocument failed: ${error}`);
			res.status(500).send(`Router - GET process/:id/datail/listDocument failed: ${error}`);
		});
}).catch((error) => {
		console.log(`Router - GET process/:id/detail/listDocument failed: ${error}`);
		res.status(500).send(`Router - GET process/:id/detail/listDocument failed: ${error}`);				
	});
})


module.exports = router;
