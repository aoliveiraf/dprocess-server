const express = require('express');
const router = express.Router();
const db = require('../config/database');
const process_func = require('../database/process')

router.use((req,res,next) => {
	next();
})

router.get('/id/:idProcess', (req, res, next) => {

	let idProcess = req.params.idProcess;
	
	  let query = `




	  FOR pp IN process FILTER pp.id == @idProcess

	  let listEventProcess = (
	  FOR ev IN 1 ANY pp._id hasEventPurpose FILTER  ev._type == 'Connection' SORT ev.startTime DESC
	  return ev
	  )
	  
		  
	  let listDocNotNull = 
		  (
		  FOR ee IN 2 ANY pp._id hasProcessDocument, hasEventPurpose FILTER ee._type=='Creation'
		  SORT ee.timeStart DESC
		 
			  FOR dd IN 1 ANY ee._id hasEventPurpose filter dd.status!='null'
				  FOR s IN 1 ANY dd._id hasDocumentSubject
					 FOR en IN 1..1 OUTBOUND ee._id hasEventSender
					    FOR sup IN 1..1 OUTBOUND en._id hasEntitySuperEntity 
                return {document:dd,subjectDocument:s,entity:en,super:sup,event:ee})

	  let listDocNull = 
		  (
		  FOR ee IN 2 ANY pp._id hasProcessDocument, hasEventPurpose FILTER ee._type=='Annulation'
		  SORT ee.timeStart DESC
			  FOR dd IN 1 ANY ee._id hasEventPurpose
				  FOR s IN 1 ANY dd._id hasDocumentSubject
					 FOR en IN 1..1 OUTBOUND ee._id hasEventSender
					    FOR sup IN 1..1 OUTBOUND en._id hasEntitySuperEntity
                return {document:dd,subjectDocument:s,entity:en,super:sup,event:ee})
		  
		 
	  let listEntity = (FOR evv IN listEventProcess 
	                    FOR en IN 1 ANY evv._id hasEventReceiver FILTER en._type == 'Administrative'
	                   
	                    return {entity:en,time:evv.timeStart})
	                    
	  
	  let subjectProcess = (FOR s IN 1 ANY pp._id hasProcessSubject return s)

// begin concerned	  
	let creator = (FOR evs IN 1 ANY pp._id hasEventPurpose FILTER evs._type == 'Creation' 
	FOR ens IN 1 ANY evs._id hasEventSender sort evs.timeStart ASC return ens)
	  
	let event_connection = (
		FOR ev IN 1 ANY pp._id hasEventPurpose FILTER ev._type=='Connection' sort ev.timeStart ASC limit 3 return ev
	)
	let concerned_et1 = flatten(
		FOR ev IN event_connection
			return DISTINCT (FOR en IN 1 ANY ev._id hasEventReceiver FILTER 
			(en._type== 'IndividualPerson' OR en._type== 'LegalPerson') AND en._key!=creator[0]._key
			return en))
		
	let concerned_et2 = flatten(
		FOR ev IN event_connection
			return DISTINCT (FOR en IN 1 ANY ev._id hasEventReceiver FILTER 
			(en._type== 'IndividualPerson' OR en._type== 'LegalPerson')  AND en._key==creator[0]._key
			return en)
	)

	let concerned = APPEND(concerned_et1,first(concerned_et2))

// end concerned


// begin administration concerned


let admConcernedBase =(for e,h in 1 ANY pp._id hasEntityCurrentProcess filter (e._type=="IndividualPerson" or e._type=="LegalPerson") and h.active == true  return e)

let admConcerned1 =(
for ev in 1 ANY pp._id hasEventPurpose filter ev._type =='Connection' sort ev.timeStart DESC
for en in 1 ANY ev._id hasEventSender filter en._key == admConcernedBase[0]._key return en)

let admConcerned2 =(
for ev in 1 ANY pp._id hasEventPurpose filter ev._type =='Connection' sort ev.timeStart DESC
for en in 1 ANY ev._id hasEventSender filter en._key == admConcernedBase[1]._key return en)

let admConcerned = UNION(admConcerned1,admConcerned2)


// end admintration caoncerned

// begin entity promise

let promiseDetail = (
for ev in 1 ANY pp._id hasEventPurpose FILTER ev._type=='PromiseConnection' and ev.active==true 
    for en in 1 ANY ev._id hasEventReceiver return {entity:en,event:ev}
) 

// end entity promise


		  return {
		  promiseDetail:promiseDetail[0],
		  admConcerned:admConcerned[0],
		  listEntity:listEntity,
		  concerned:concerned[0],
		  process:pp,
		  subjectProcess:subjectProcess[0],
		  listDocNull:listDocNull,
		  listDocNotNull:listDocNotNull
		  }
		  `;
	db.query(query,{idProcess:idProcess})
		.then((cursor) => {
			cursor.all()
			.then((result) => { 
				res.status(200).send(result[0])
			})
			.catch((error) => {
				console.log(`Router - /search/process/:idProcess failed: ${error}`);
				res.status(500).send(`Router - /search/process/:idProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - /search/process/:idProcess failed: ${error}`);
			res.status(500).send(`Router - /search/process/:idProcess failed: ${error}`);
		});
});

router.put('/', async(req, res, next) => {
	let id_super = `entity/${req.body.id_super}`;
	let id_process= req.body.id_process;
    let partReqName= req.body.partReqName;
    let partIndName=req.body.partIndName;
    let partAdmName = req.body.partAdmName
	let partNameSubject=req.body.partNameSubject;
	let processActive=req.body.processActive;


	let super_super = await db.query(`
	FOR super_super IN 1 OUTBOUND @id_super hasEntitySuperEntity 
	return {id_super_super:super_super._id} 
	`,{id_super:id_super});

	let result = await process_func.getFullDatail(
							id_process,
							super_super._result[0].id_super_super,
							"",
							"",
							partReqName,
							partIndName,
							partAdmName,
							partNameSubject,
							"",
							processActive
							)
	res.send(result);
})
/*
router.put('/', (req, res, next) => {
	let key_super = req.body.key_super;
	let partRequesterName = req.body.partName;
	
	  let query = `
	  FOR super IN 1 INBOUND @id_super_super hasEntitySuperEntity 
	  FOR p,h IN 1 ANY super._id hasEntityCurrentProcess FILTER h.active==true and (CONTAINS(LOWER(p.id), LOWER(@id)) || @id=="")
	  
	  FOR req,hreq IN 1 INBOUND p._id hasEntityCurrentProcess FILTER hreq.concern=="Requester" 
	  FILTER CONTAINS(LOWER(req.name), LOWER(@searchName)) || @searchName == ""
	  FOR ind,hind IN 1 INBOUND p._id hasEntityCurrentProcess FILTER hind.concern=="IndividualPerson"
	  FILTER CONTAINS(LOWER(ind.name), LOWER(@searchName)) || @searchName == ""
	  FOR adm,hadm IN 1 INBOUND p._id hasEntityCurrentProcess FILTER hadm.concern=="Administrative"
	  FILTER CONTAINS(LOWER(adm.name), LOWER(@searchName)) || @searchName == ""
	  
	  
	  FOR s IN 1 ANY p._id ANY hasProcessSubject FILTER CONTAINS(LOWER(s.name), LOWER(@partNameSubject)) || @partNameSubject == ""
	  
	  return {process:p,requester:req,individual:ind,administrative:adm}
	  `;
	db.query(query,{idProcess:idProcess})
		.then((cursor) => {
			cursor.all()
			.then((result) => { 
				res.status(200).send(result[0])
			})
			.catch((error) => {
				console.log(`Router - /search/process/:idProcess failed: ${error}`);
				res.status(500).send(`Router - /search/process/:idProcess failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - /search/process/:idProcess failed: ${error}`);
			res.status(500).send(`Router - /search/process/:idProcess failed: ${error}`);
		});
});
*/


module.exports = router;
