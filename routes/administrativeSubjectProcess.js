const express = require('express');
const router = express.Router();
const db = require('../config/database');

router.post('/', async (req, res) => {

	let data = req.body;

	const id_super_super = 'entity/0'; // prefeitura

	try {
		const result = await db.query(`
			INSERT {name:@name} INTO subjectProcess LET new_subject = NEW
		  	INSERT {_from:@id_super_super,_to:new_subject._id} IN hasEntitySubjectProcess
	   RETURN new_subject`,
			{ name: data.name, id_super_super: id_super_super })


		res.status(200).send(result._result[0]);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess failed: ${error}`);
		res.status(500).send(result);
	}

})

router.put('/', (req, res) => {

	let data = req.body;

	db.query(
		`UPDATE @data._key WITH {name:@data.name} IN subjectProcess let new_subject = NEW
		   RETURN new_subject`,
		{ data: data }
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result[0]))
			.catch((error) => {
				console.log(`Router - PUT /subjectProcess failed: ${error}`);
				res.status(500).send(`Router - PUT /subjectProcess failed: ${error}`);
			});
	})
		.catch((error) => {
			console.log(`Router - PUT /subjectProcess failed: ${error}`);
			res.status(500).send(`Router - PUT /subjectProcess failed: ${error}`);
		});
})

router.post('/canEntityConnectSubjectProcess', (req, res, next) => {

	const id_entity = `entity/${req.body.id_entity}`;
	const id_subjectProcess = `subjectProcess/${req.body.id_subjectProcess}`;
	const destinationProcess = req.body.destinationProcess;
	const _listTypeDocumentCreation = req.body._listTypeDocumentCreation;
	const canCreateProcess = req.body.canCreateProcess;


	delete req.body.id_entity;
	delete req.body.id_subjectProcess;
	db.query(
		`INSERT {_from:@id_entity,_to:@id_subjectProcess,
			_listTypeDocumentCreation:@_listTypeDocumentCreation,
			canCreateProcess:@canCreateProcess,destinationProcess:@destinationProcess} INTO canEntityConnectSubjectProcess LET new_has = NEW
	   RETURN new_has`,
		{
			id_subjectProcess: id_subjectProcess,
			id_entity: id_entity,
			_listTypeDocumentCreation: _listTypeDocumentCreation,
			canCreateProcess: canCreateProcess,
			destinationProcess: destinationProcess
		}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
				console.log(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
				res.status(500).send(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
			});
	})
		.catch((error) => {
			console.log(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
			res.status(500).send(`Router - POST /subjectProcess/canEntityConnectSubjectProcess failed: ${error}`);
		});
})

router.get('/listSubjectProcess/listSuper/beginProcess', async (req, res) => {

	const id_super_super = 'entity/0'; // prefeitura
	let searchName = req.query.searchName;

	if (searchName == undefined) {
		searchName = ""
	}

	try {
		const result = await db.query(`
		for s in 1 any @id_super_super hasEntitySubjectProcess
		FILTER    
		(CONTAINS(LOWER(s.name), LOWER(@searchName)) 
		||@searchName == '') 
		SORT s.name 		
		let listSuper = 
		(FOR e,can in 1 INBOUND s._id canEntityConnectSubjectProcess FILTER can.destinationProcess == 'beginProcess'
		FOR es IN 1 OUTBOUND e._id hasEntitySuperEntity 
		return distinct es
		)
		return {subjectProcess:s,listSuper:listSuper}
				
`, { id_super_super: id_super_super, searchName: searchName })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess/listSubject/ListSuper failed: ${error}`);
		res.status(500).send(result);
	}

})

router.get('/listSubjectProcess/listSuper/endProcess', async (req, res) => {

	const id_super_super = 'entity/0'; // prefeitura
	let searchName = req.query.searchName;

	if (searchName == undefined) {
		searchName = ""
	}

	try {
		const result = await db.query(`
		for s in 1 any @id_super_super hasEntitySubjectProcess
		FILTER    
		(CONTAINS(LOWER(s.name), LOWER(@searchName)) 
		||@searchName == '') 
		SORT s.name 		
		let listSuper = 
		(FOR e,can in 1 INBOUND s._id canEntityConnectSubjectProcess FILTER can.destinationProcess == 'endProcess'
		FOR es IN 1 OUTBOUND e._id hasEntitySuperEntity 
		return distinct es
		)
		return {subjectProcess:s,listSuper:listSuper}
				
`, { id_super_super: id_super_super, searchName: searchName })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess/listSubject/ListSuper failed: ${error}`);
		res.status(500).send(result);
	}

})

router.get('/subjectProcess/:key_subjectProcess/listSuper/beginProcess', async (req, res) => {
	const id_subjectProcess = `subjectProcess/${req.params.key_subjectProcess}`;
	console.log(id_subjectProcess);

	try {
		const result = await db.query(`
		FOR e,can in 1  ANY @id_subjectProcess canEntityConnectSubjectProcess FILTER can.destinationProcess == 'beginProcess'
		FOR es IN 1 OUTBOUND e._id hasEntitySuperEntity 
		return distinct es								
`, { id_subjectProcess: id_subjectProcess })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /subjectProcess/:key_subjectProcess/listSuper/endProcess failed: ${error}`);
		res.status(500).send(result);
	}

})

router.get('/subjectProcess/:key_subjectProcess/listSuper/endProcess', async (req, res) => {

	const id_subjectProcess = `subjectProcess/${req.params.key_subjectProcess}`;

	try {
		const result = await db.query(`
		FOR e,can in 1  ANY @id_subjectProcess canEntityConnectSubjectProcess FILTER can.destinationProcess == 'endProcess'
		FOR es IN 1 OUTBOUND e._id hasEntitySuperEntity 
		return distinct es								
						
`, { id_subjectProcess: id_subjectProcess })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /subjectProcess/:key_subjectProcess/listSuper/endProcess failed: ${error}`);
		res.status(500).send(result);
	}

})

router.get('/fit', (req, res, next) => {

	let searchName = req.query.searchName;

	let query = `FOR s IN subjectProcess SORT s.name FILTER (LOWER(s.name) == LOWER(@searchName) or @searchName == '') RETURN s`;
	db.query(query, { searchName: searchName })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET /administrativeSubjectProcess/fit failed: ${error}`);
					res.status(500).send(`Router - GET /administrativeSubjectProcess/fit failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /administrativeSubjectProcess/fit failed: ${error}`);
			res.status(500).send(`Router - GET /administrativeSubjectProcess/fit failed: ${error}`);
		});
});

router.get('/', async (req, res) => {

	const id_super_super = 'entity/0'; // prefeitura
	let searchName = req.query.searchName;

	if (searchName == undefined) {
		searchName = ""
	}
	try {
		const result = await db.query(`
			for s in 1 any @id_super_super hasEntitySubjectProcess
			FILTER    
			(CONTAINS(LOWER(s.name), LOWER(@searchName)) 
			||@searchName == '') 
			SORT s.name 			
			
			return s`,

			{ id_super_super: id_super_super, searchName: searchName })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess failed: ${error}`);
		res.status(500).send(result);
	}

})

router.get('/toNew', async (req, res) => {

	const id_super_super = 'entity/0'; // prefeitura
	try {
		const result = await db.query(`
		for s in 1 any @id_super_super hasEntitySubjectProcess
		filter LENGTH(FOR ee IN 1 ANY s._id canEntityConnectSubjectProcess return ee)==0
		return s`,

			{ id_super_super: id_super_super })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess failed: ${error}`);
		res.status(500).send(result);
	}

})


router.get('/subjectProcess/:key_subject/entity/:key_super/canConnect', (req, res) => {
	let id_subject = `subjectProcess/${req.params.key_subject}`;
	let id_super = `entity/${req.params.key_super}`;

	let query = `
	FOR e IN 1 INBOUND @id_super hasEntitySuperEntity FILTER e._id SORT e.name
	FOR ee,h IN 1 ANY @id_subject canEntityConnectSubjectProcess FILTER ee._id == e._id  
	
	RETURN {entity:ee,has:h}
	`;
	db.query(query, { id_super: id_super, id_subject: id_subject })
		.then((cursor) => {
			cursor.all()
				.then((result) => { res.status(200).send(result); })
				.catch((error) => {
					console.log(`Router GET /subjectProcess/:key_subject/entity/:key_entity/canConnect failed: ${error}`);
					res.status(500).send(`Router - GET /subjectProcess/:key_subject/entity/:key_entity/canConnect failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /subjectProcess/:key_subject/entity/:key_entity/canConnect failed: ${error}`);
			res.status(500).send(`Router - GET /subjectProcess/:key_subject/entity/:key_entity/canConnect failed: ${error}`);
		});
});

router.get('/toEdit', async (req, res) => {

	const id_super_super = 'entity/0'; // prefeitura
	let searchName = req.query.searchName;

	if (searchName == undefined) {
		searchName = ""
	}

	try {
		const result = await db.query(`
		for s in 1 any @id_super_super hasEntitySubjectProcess
		FILTER    
		(CONTAINS(LOWER(s.name), LOWER(@searchName)) 
		||@searchName == '') 
		SORT s.name 		
		let result =
		(
			FOR es in 2 INBOUND s._id canEntityConnectSubjectProcess, OUTBOUND hasEntitySuperEntity 
		
			let listEntity = 
			(
				FOR esd in 1 INBOUND es._id hasEntitySuperEntity 
				FILTER LENGTH(FOR esdf in 1 INBOUND s._id canEntityConnectSubjectProcess FILTER esdf._key == esd._key return esdf)>0
				return esd
			)
		return {super:es,listEntity:listEntity})
		
		return {subjectProcess:s,listDetailEntity:result}
		
`, { id_super_super: id_super_super, searchName: searchName })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess/toEdit failed: ${error}`);
		res.status(500).send(result);
	}

})


router.get('/listSubjectProcess/listSuper', async (req, res) => {

	const id_super_super = 'entity/0'; // prefeitura
	let searchName = req.query.searchName;

	if (searchName == undefined) {
		searchName = ""
	}

	try {
		const result = await db.query(`
		for s in 1 any @id_super_super hasEntitySubjectProcess
		FILTER    
		(CONTAINS(LOWER(s.name), LOWER(@searchName)) 
		||@searchName == '') 
		SORT s.name 		
		let listSuperEndProcess = 
		(FOR e,can in 1  ANY s._id canEntityConnectSubjectProcess FILTER can.destinationProcess == 'endProcess'
		FOR es IN 1 OUTBOUND e._id hasEntitySuperEntity 
		return distinct es
		)
		let listSuperBeginProcess = 
		(FOR e,can in 1  ANY s._id canEntityConnectSubjectProcess FILTER can.destinationProcess == 'beginProcess'
		FOR es IN 1 OUTBOUND e._id hasEntitySuperEntity 
		return distinct es
		)

		return {subjectProcess:s,listSuperBeginProcess:listSuperBeginProcess,listSuperEndProcess:listSuperEndProcess}
				
`, { id_super_super: id_super_super, searchName: searchName })

		res.status(200).send(result._result);
	} catch (error) {
		console.log(`Router - POST /administrativeSubjectProcess/listSubject/ListSuper failed: ${error}`);
		res.status(500).send(result);
	}

})

router.get('/:key_entity/process/datail/pending', (req, res, next) => {
	let id_entity = `entity/${req.params.key_entity}`;

	let query = `
	let listSubSuper = (FOR super IN 1 OUTBOUND @id_entity hasEntitySuperEntity
		FOR s IN 1 ANY super._id canEntityConnectSubjectProcess 
		return s)
		
		
		let super = (FOR super IN 1 OUTBOUND @id_entity hasEntitySuperEntity return super)
		
		let listDetail = (
		FOR super_super IN 2 OUTBOUND @id_entity hasEntitySuperEntity, hasEntitySuperEntity
		FOR ep IN 1 INBOUND super_super._id hasEventReceiver 
		FILTER ep._type == 'PromiseConnection' and 	ep.active==true  
		FOR p IN 1 OUTBOUND ep._id hasEventPurpose
		FOR s IN 1 ANY p._id hasProcessSubject FILTER CONTAINS(listSubSuper,s)
		FOR en,h IN 1 ANY p._id hasEntityCurrentProcess FILTER h.requester==true
			return {
			concerned:en,
			process:p,subjectProcess:s,
			event:ep
		}
		)
		return listDetail
		`;
	db.query(query, { id_entity: id_entity })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result[0]))
				.catch((error) => {
					console.log(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
					res.status(500).send(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
			res.status(500).send(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
		});
});


router.put('/canConnect_IN_AND_OUT', (req, res, next) => {

	let id_logged = `entity/${req.body.key_logged}`;
	let id_subjectProcess = `subjectProcess/${req.body.key_subjectProcess}`;
	let listProcess = req.body.listProcess;

	console.log(id_logged);
	console.log(id_subjectProcess);
	console.log(listProcess);
	
	
	

	let query = `
	let super_login = (FOR super in  1 OUTBOUND @id_logged hasEntitySuperEntity return super)

	let super_super_login = (FOR super in  1 OUTBOUND super_login[0]._id hasEntitySuperEntity return super)

	let listResult = (
	FOR id_process IN @listProcess
	FOR event IN 1 ANY id_process hasEventPurpose FILTER event.active == false AND event._type == "PromiseConnection" SORT event.time DESC
	FOR sender IN 1 ANY event._id hasEventSender FILTER sender._type == 'Administrative'
	FOR super_sender IN 1 OUTBOUND sender._id hasEntitySuperEntity FILTER super_sender._id != super_super_login[0]._id
	return super_sender)
	
	let result1 = length(listResult) == length(@listProcess)?listResult[0]:[]
	
	let listEndProcess = (
	FOR e,can IN 1 ANY @id_subjectProcess canEntityConnectSubjectProcess FILTER can.destinationProcess == 'endProcess' 
	FOR super_super IN 1 OUTBOUND e._id hasEntitySuperEntity FILTER super_super._id != super_super_login[0]._id AND super_super._id != result1._id return DISTINCT super_super) 
	
	let listEntitySub = (FOR en IN 1 ANY @id_subjectProcess canEntityConnectSubjectProcess return en)
	let listLevelDown = (
	FOR super_super in  2 OUTBOUND @id_logged hasEntitySuperEntity, hasEntitySuperEntity
	FOR siblings IN 1 INBOUND super_super._id hasEntitySuperEntity
	FILTER siblings._id != super_login[0]._id AND CONTAINS(listEntitySub,siblings) return siblings
	)

	return APPEND(APPEND(listEndProcess,result1),listLevelDown) 
			`;
	db.query(query, {
		id_logged: id_logged,
		listProcess: listProcess,
		id_subjectProcess: id_subjectProcess
	})
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result[0]))
				.catch((error) => {
					console.log(`Router - GET /administrative/canConnect_IN_AND_OUT failed: ${error}`);
					res.status(500).send(`Router - GET /administrative/canConnect_IN_AND_OUT failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /administrativeSubjectProcess/canConnect_IN_AND_OUT failed: ${error}`);
			res.status(500).send(`Router - GET /administrativeSubjectProcess/canConnect_IN_AND_OUT failed: ${error}`);
		});
});



module.exports = router;
