const express = require('express');
const router = express.Router();
const db = require('../config/database');
const schema = require('../models/Administrative');
const joi = require('joi');

const type = "Administrative";

router.post('/', (req, res, next) => {
	let body = req.body;
	body._type = type;

	const validate = joi.validate(body, schema);

	if (validate.error) {
		res.status(400).send({ 'error': validate.error.toString(), 'schema': joi.describe(schema) });
	} else {
		let id_creator = `entity/${body.id_creator}`;
		let id_super = `entity/${body.id_super}`;
		delete body.id_creator;
		delete body.id_super;

		db.query(`
		INSERT @body INTO entity LET new_entity = NEW
		
		INSERT 	{_type:@_type,timeStart:@time,timeEnd:@time,active:true} INTO event let new_event = NEW
		INSERT {_from:new_event._id,_to:@id_creator} INTO hasEventSender
		INSERT {_from:new_event._id,_to:@id_neutral} INTO hasEventReceiver
		INSERT {_from:new_event._id,_to:new_entity._id} INTO hasEventPurpose

		INSERT { _from:new_entity._id, _to:@id_super} INTO hasEntitySuperEntity

		RETURN new_entity`,
			{ body: body, id_neutral: 'neutral/0', id_creator: id_creator, id_super: id_super, _type: "Creation", time: Date.now() })
			.then((cursor) => {
				cursor.all().then((result) => {
					res.status(200).send(result);
				})
					.catch((error) => {
						console.log(`Router - POST /administrative failed: ${error}`);
						res.status(500).send(`Router - POST /administrative failed: ${error}`);
					});
			})
			.catch((error) => {
				console.log(`Router - POST /administrative failed: ${error}`);
				res.status(500).send(`Router - POST /administrative failed: ${error}`);
			});
	}
});

router.put('/', (req, res, next) => {
	let data = req.body;

	//	const validate = joi.validate(body, schema);

	//	if (validate.error) {
	//		res.status(400).send({ 'error': validate.error.toString(), 'schema':joi.describe(schema)});
	//	} else {
	let id_creator = `entity/${data.id_creator}`;
	delete data.id_creator;

	db.query(
		`UPDATE @data._key WITH @data IN entity let new_entity = NEW

		INSERT {_type:'Update',timeStart:@time, active:true} INTO event let new_event = NEW
		INSERT { _from:new_event._id, _to:@id_creator} INTO hasEventSender
		INSERT { _from:new_event._id, _to:new_entity._id} INTO hasEventPurpose

		   RETURN new_entity`,
		{ data: data, id_creator: id_creator, time: Date.now() })
		.then((cursor) => {
			cursor.all().then((result) => {
				res.status(200).send(result);
			})
				.catch((error) => {
					console.log(`Router - PUT /administrative failed: ${error}`);
					res.status(500).send(`Router - PUT /administrative failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - PUT /administrative failed: ${error}`);
			res.status(500).send(`Router - PUT /administrative failed: ${error}`);
		});
	//	}
});

router.get('/', (req, res, next) => {
	//	let filters = filter.catchParams('entity', 'e', req.params);
	//	let query = `FOR e IN entity ${filters} RETURN e`;
	let searchName = req.query.searchName;

	let query = `FOR e IN entity filter e._type == @type and CONTAINS(e.name, @searchName) RETURN e`;
	db.query(query, { type: type, searchName: searchName })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router entity/individualPerson - GET /entity/individualPerson failed: ${error}`);
					res.status(500).send(`Server Interval Error`);
				});
		})
		.catch((error) => {
			console.log(`Router entity/individualPerson - GET /entity/individualPerson failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});


module.exports = router;
