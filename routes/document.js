var express = require('express');
var router = express.Router();
var db = require('../config/database');
var Joi = require('joi');
const PDFDocument = require('pdfkit');
const fs = require('fs');
const pdf = require('html-pdf');


const mainSchema = require('../models/Document');


router.use((req, res, next) => {
	next();
})

router.get('/:id', (req, res, next) => {
	let id = req.params.id;
	db.collection('document').firstExample({ _key: id })
		.then((result) => res.status(200).send(result))
		.catch((error) => {
			console.log(`Router Document - GET /document failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});


async function updateDocumentPath(idDocument, path) {
	db.query(`
			FOR d IN document FILTER d._id == @id_document  
                UPDATE d._key WITH {path:@path} IN document let new = NEW
		return new`,
		{
			id_document: idDocument
			, path: path
		})
		.then((cursor) => {

			cursor.all().then((result) => {
				console.log('Seccess', result)
			}
			)
				.catch((error) => console.log(`SubRouter - POST /updateDocumentPath failed: ${error}`));
		})
		.catch((error) => console.log(`SubRouter - POST /updateDocumentPath failed: ${error}`));

}

router.post('/', (req, res, next) => {
	let body = req.body;
	//	console.log('body', body);

	if (body.text === null) {
		body.text = "";
	}


	const validate = Joi.validate(body, mainSchema);
	if (validate.error) {
		res.status(400).send({ 'error': validate.error.toString(), 'model': Joi.describe(mainSchema) });
		console.log('error!', Joi.describe(mainSchema));

	} else {
		let id_creator = `entity/${body.id_creator}`;
		let id_super = `entity/${body.id_super}`
		let id_process = `process/${body.id_process}`;
		let id_subject = `subjectDocument/${body.id_subject}`;
		body.active = true;
		delete body.id_creator;
		delete body.id_process;
		delete body.id_subject;
		delete body.id_super;


		db.query(`
		INSERT @body INTO document LET new_document = NEW

		INSERT {_type:@_type,time:@time, active:true} IN event let new_event = NEW
		INSERT {_from:new_event._id,_to:new_document._id} INTO hasEventPurpose

		INSERT {_from:new_document._id,_to:@id_subject} INTO hasDocumentSubject

        INSERT {_from:new_document._id,_to:@id_process} INTO hasProcessDocument
		let listData = [@id_creator,@id_super]
		FOR d IN listData
		   INSERT {_from:new_event._id,_to:d} INTO hasEventSender

		RETURN new_document`,
			{ body: body, id_super: id_super, id_subject: id_subject, id_creator: id_creator, _type: 'Creation', id_process: id_process, time: Date.now() })
			.then((cursor) => {
				cursor.all().then((result) => {
					let newDoc = result[0];
					updateIfFilingHasCurrentProcess(newDoc._id);
					res.status(200).send(result);

				}).catch((error) => {
					console.log(`Router Document - POST /document failed: ${error}`);
					res.status(500).send(`Router Document - POST /document failed: ${error}`);
				});
			})
			.catch((error) => {
				console.log(`Router Document - POST /document failed: ${error}`);
				res.status(500).send(`Router Document - POST /document failed: ${error}`);
			});
	}
});

async function centerDocumentHeader(id_creator) {
	return db.query(`
		for es in any @id_creator hasEntitySuperEntity
		for e in ANY shortest_path es._id to 'entity/0' hasEntitySuperEntity return e`,
		{
			id_creator: id_creator
		})

}

async function updateIfFilingHasCurrentProcess(idDocument) {

	//	console.log('idDocument in updateOneHasCurrentProcess', idDocument);

	await db.query(`
			FOR pd IN 1 ANY @id_document hasProcessDocument 
				FOR s IN 1 ANY @id_document hasDocumentSubject FILTER s._type == @_typeDoc
			   		FOR pp, hpp IN 1 ANY pd._id hasEntityCurrentProcess	FILTER hpp.requester==false and hpp.active == true	
						UPDATE hpp._key WITH {active:false} IN hasEntityCurrentProcess let new = NEW
						
		return new`,
		{
			id_document: idDocument
			, _typeDoc: 'Filing'
		})

	await db.query(`
		FOR pd IN 1 ANY @id_document hasProcessDocument 
			FOR s IN 1 ANY @id_document hasDocumentSubject FILTER s._type == @_typeDoc
				   UPDATE pd._key WITH {active:false} IN process let new = NEW	
					
			return new`,
		{
			id_document: idDocument
			, _typeDoc: 'Filing'
		})

}

router.post('/annulation', (req, res, next) => {
	let body = req.body;

	//	const validate = Joi.validate(body, mainSchema);
	//	if (validate.error) {
	//		res.status(400).send({ 'error': validate.error.toString(), 'model': Joi.describe(mainSchema) });
	//		console.log('error!', Joi.describe(mainSchema));

	//	} else {
	let id_creator = `entity/${body.key_creator}`;
	let id_document = `document/${body.key_document}`;
	let id_super = `entity/${body.key_super}` // conecte after...
	let key_document = body.key_document;
	let justification = body.justification;

	db.query(`

		INSERT {_type:'Annulation',time:@time, active:true, justification:@justification} IN event let new_event = NEW
		INSERT {_from:new_event._id,_to:@id_document} INTO hasEventPurpose
		UPDATE @key_document WITH {active:'false'} IN document let new_document = NEW

		FOR d in [@id_creator,@id_super]
			INSERT {_from:new_event._id,_to:d} INTO hasEventSender
		
		RETURN new_document`,
		{ justification: justification, key_document: key_document, id_document: id_document, id_creator: id_creator, id_super: id_super, time: Date.now() })
		.then((cursor) => {
			cursor.all().then((result) => {
				res.status(200).send(result);
			}).catch((error) => {
				console.log(`Router - POST /document/annulation failed: ${error}`);
				res.status(500).send(`Router - POST /document/annulation failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - POST /document/annulation failed: ${error}`);
			res.status(500).send(`Router - POST /document/annulation failed: ${error}`);
		});
	//	}
});


module.exports = router;
