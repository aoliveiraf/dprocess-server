const express = require('express');
const router = express.Router();
const db = require('../config/database');
const schema = require('../models/IndividualPersonOut');
const joi = require('joi');

const type = "IndividualPerson";

router.post('/', (req, res, next) => {
	let body = req.body;
	let id_address = `address/${body.id_address}`;
	let complemento = body.complemento;
	let firstId = body.firstId;

	body._type=type;
	body.id_super="-3" // entity OUT

	const validate = joi.validate(body, schema);

//	if (validate.error) {
//		console.log('error', validate.error.toString())
//		res.status(400).send({ 'error': validate.error.toString(), 'schema':joi.describe(schema)});
//	} else {
		let id_creator = `entity/${body.id_creator}`;
		let id_super = `entity/${body.id_super}`;
		delete body.id_creator;
		delete body.id_super;
		delete body.id_address;
		delete body.complemento;
		delete body.firstId;

		db.query(`
		INSERT @body INTO entity LET new_entity = NEW
		INSERT {_from:new_entity._id,_to:@id_address,complemento:@complemento,firstId:@firstId} IN hasEntityAddress 
		INSERT { _from:new_entity._id, _to:@id_super} INTO hasEntitySuperEntity
		INSERT {_type:'Creation',timeStart:@time,timeEnd:@time, active:true} INTO event let new_event = NEW
		INSERT { _from:new_event._id, _to:'neutral/0'} INTO hasEventSender
		INSERT { _from:new_event._id, _to:new_entity._id} INTO hasEventPurpose

		RETURN new_entity`, 
//		{body:body,id_address:id_address,complemento:complemento,firstId:firstId,_type:'Creation',id_creator:id_creator,id_super:id_super,time:Date.now()})
{body:body,id_address:id_address,complemento:complemento,firstId:firstId,id_super:id_super,time:Date.now()})

.then((cursor) => {
			cursor.all().then((result) => {
					res.status(200).send(result[0]);
				})
				.catch((error) => {
					console.log(`Router individualPersonIn - POST /individualPersonIn failed: ${error}`);
					res.status(500).send(`Router individualPersonIn - POST /individualPersonIn failed: ${error}`);				
				});
			})
			.catch((error) => {
				console.log(`Router individualPersonIn - POST /individualPErsonIn failed: ${error}`);
				res.status(500).send(`Router individualPersonIn - POST /individualPersonIn failed: ${error}`);	
			});
//	}
});

router.put('/', (req, res, next) => {
	let body = req.body;
	let id_address = `address/${body.id_address}`;
	let id_entity = `entity/${body._key}`
	let complemento = body.complemento;
	let firstId = body.firstId;
	let key_entity = body._key;
	body._type=type;
	body.id_super="-3" // entity OUT


	const validate = joi.validate(body, schema);

//	if (validate.error) {
//		console.log('error', validate.error.toString())
//		res.status(400).send({ 'error': validate.error.toString(), 'schema':joi.describe(schema)});
//	} else {
		let id_creator = `entity/${body.id_creator}`;
		delete body.id_creator;
		delete body.id_super;
		delete body.id_address;
		delete body.complemento;
		delete body.firstId;
		delete body._key;

		db.query(`
			FOR add,h IN 1 ANY @id_entity hasEntityAddress
			UPDATE h._key WITH {_from:@id_entity,_to:@id_address,complemento:@complemento,firstId:@firstId} IN hasEntityAddress

			UPDATE @key_entity WITH @body IN entity let new_entity = NEW
			INSERT {_type:'Update',timeStart:@time, active:true} INTO event let new_event = NEW
			INSERT { _from:new_event._id, _to:@id_creator} INTO hasEventSender
			INSERT { _from:new_event._id, _to:new_entity._id} INTO hasEventPurpose

		RETURN new_entity`, 
		{body:body,key_entity:key_entity,id_entity:id_entity,id_address:id_address,complemento:complemento,firstId:firstId,id_creator:id_creator,time:Date.now()})
		.then((cursor) => {
			cursor.all().then((result) => {
					res.status(200).send(result[0]);
				})
				.catch((error) => {
					console.log(`Router individualPersonIn - PUT /individualPerson failed: ${error}`);
					res.status(500).send(`Router individualPersonIn - PUT /individualPerson failed: ${error}`);				
				});
			})
			.catch((error) => {
				console.log(`Router individualPersonIn - PUT /individualPersonIn failed: ${error}`);
				res.status(500).send(`Router individualPersonIn - PUT /individualPerson failed: ${error}`);	
			});
//	}
});

router.get('/', (req, res, next) => {
	let searchName = req.query.searchName;

  	let query = `FOR e IN entity filter e._type == @type and CONTAINS(e.name, @searchName) RETURN e`;
	db.query(query,{type:type,searchName:searchName})
		.then((cursor) => {
			cursor.all()
			.then((result) => res.status(200).send(result))
			.catch((error) => {
				console.log(`Router entity/individualPerson - GET /entity/individualPerson failed: ${error}`);
				res.status(500).send(`Server Interval Error`);
			});
		})
		.catch((error) => {
			console.log(`Router entity/individualPerson - GET /entity/individualPerson failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});


module.exports = router;
