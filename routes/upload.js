const fs = require("fs")
const formidable = require("formidable")
const express = require('express');
const router = express.Router()
var db = require('../config/database');

router.get('/document/:keyDocument',(req,res)=>{
    let keyDocument = req.params.keyDocument;
    
    db.query('FOR d IN document FILTER d._key == @keyDocument return d',{keyDocument:keyDocument})
    .then((cursor)=>{
        cursor.all()
        .then((result)=>{
            let path = `${__dirname}/uploads/${result[0].path}`;
            res.status(200).sendFile(path);
        })
    }).catch((error)=>{
        res.status(500).send(`Router GET - /document/:keyDocument failed: ${error}`);
    })
   // let fileName = `documento.${keyDocument}.assunto.${keySubject}`;

//    let path = `${__dirname}/uploads/2633877/documento.2634178.assunto.2228079.pdf`;

//    res.status(200).sendFile(path);
})

router.post('/',(req,res)=>
{
    let keyProcess = req.query.keyProcess;
    let fileName = req.query.fileName;
    let keyDocument = req.query.keyDocument;
    let path = `${keyProcess}/${fileName}.pdf`

    db.query(`FOR d IN document FILTER d._key ==@keyDocument
              UPDATE d._key WITH {path:@path} IN document`,
    {keyDocument:keyDocument,path:path})
    .then((cursor)=>{
        cursor.all()
        .then((result)=>{
            var form = new formidable.IncomingForm();

            let existDir = fs.existsSync(`${__dirname}/uploads/${keyProcess}`)
            if(!existDir) {
                console.log('não há',`${__dirname}/uploads/${keyProcess}`);
                
                fs.mkdirSync(`${__dirname}/uploads/${keyProcess}`)
            }
        
            form.parse(req);
            
            form.on('fileBegin', function (name, file){
                file.path = `${__dirname}/uploads/${path}`;
             //   file.path = `${__dirname}/uploads/${file.name}`;
            });    
    
            form.on('end', function() {
                res.status(200).send(result);
            });    

            form.on('errror', function(err) {
                console.log('error',err);
                req.resume();
            });    

        }).catch((error)=>{
            res.status(500).send(`Router - /upload failed: ${error}`);
        })
    }).catch((error)=>{
        res.status(500).send(`Router - /upload failed: ${error}`);

    })

    

/*    form.on("progress", (rec, exp) => {
        let total = (rec / exp) * 100
        console.log('progress:',total);
        
        //io.emit(token, { "percent": parseInt(total) })
    })

    form.on('file', function (name, file){
        console.log(name + file.name);
    });

    res.status(200).send(__dirname + '/upload');
*/
})

module.exports = router;

