const express = require('express');
const router = express.Router();
const db = require('../config/database');
const schema = require('../models/IndividualPersonIn');
const joi = require('joi');

const type = "IndividualPersonIn";

router.post('/', (req, res, next) => {
	let body = req.body;
	body._type=type;

	const validate = joi.validate(body, schema);

	if (validate.error) {
		res.status(400).send({ 'error': validate.error.toString(), 'schema':joi.describe(schema)});
	} else {
		let id_creator = `entity/${body.id_creator}`;
		let id_super = `entity/${body.id_super}`;
		delete body.id_creator;
		delete body.id_super;

		db.query(`
		INSERT @body INTO entity LET new_entity = NEW
		INSERT { _from:new_entity._id, _to:@id_super} INTO hasEntitySuperEntity
		INSERT {_type:@_type,timeStart:@time,timeEnd:@time, active:true} INTO event let new_event = NEW
		INSERT { _from:new_event._id, _to:@id_creator} INTO hasEventSender
		INSERT { _from:new_event._id, _to:new_entity._id} INTO hasEventPurpose
		RETURN new_entity`, 
		{body:body,_type:'Creation',id_creator:id_creator,id_super:id_super,time:Date.now()})
		.then((cursor) => {
			cursor.all().then((result) => {
					res.status(200).send(result);
				})
				.catch((error) => {
					console.log(`Router individualPersonIn - POST /individualPersonIn failed: ${error}`);
					res.status(500).send(`Server Interval Error`);				
				});
			})
			.catch((error) => {
				console.log(`Router individualPersonIn - POST /individualPErsonIn failed: ${error}`);
				res.status(500).send(`Server Interval Error`);	
			});
	}
});

router.get('/', (req, res, next) => {
//	let filters = filter.catchParams('entity', 'e', req.params);
//	let query = `FOR e IN entity ${filters} RETURN e`;
	let searchName = req.query.searchName;

  	let query = `FOR e IN entity filter e._type == @type and CONTAINS(e.name, @searchName) RETURN e`;
	db.query(query,{type:type,searchName:searchName})
		.then((cursor) => {
			cursor.all()
			.then((result) => res.status(200).send(result))
			.catch((error) => {
				console.log(`Router entity/individualPerson - GET /entity/individualPerson failed: ${error}`);
				res.status(500).send(`Server Interval Error`);
			});
		})
		.catch((error) => {
			console.log(`Router entity/individualPerson - GET /entity/individualPerson failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});


module.exports = router;
