const process_func = require('../database/process')
const express = require('express');
const router = express.Router();
const db = require('../config/database');
//var filter = require('./filter');
const joi = require('joi');
const entity_db = require('../database/entity')

const schemaEntity = joi.object().keys({
	name: joi.string().required(),
	id: joi.string().required(),
	id_creator: joi.string().required(),
	id_super: joi.string().required()
});

router.use((req, res, next) => {

	next();
})

router.get('/', (req, res, next) => {
	let query = `FOR e IN entity RETURN e`;
	db.query(query)
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router Entities - GET /entities failed: ${error}`);
					res.status(500).send(`Server Interval Error`);
				});
		})
		.catch((error) => {
			console.log(`Router Entities - GET /entities failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});
router.post('/', (req, res, next) => {
	let body = req.body;
	const validate = Joi.validate(body, schemaEntity);
	if (validate.error) {
		res.status(400).send({ 'error': validate.error.toString(), 'model': { name: 'string', id: 'string', id_creatorEntity: 'string' } });
	} else {
		let id_creator = `entity/${body.id_creator}`;
		let id_super = `entity/${body.id_super}`;
		delete body.id_creator;

		db.query(`INSERT @body INTO process LET new = NEW
		INSERT {id_creator:@id_creator,timeStart:@time,timeEnd:@time, active:true} INTO event let new_event = NEW
		INSERT { _from:new._id, _to:new_event._id} INTO hasNewEntityEvent
		INSERT { _from:new._id, _to:@id_super} INTO hasEntitySuperEntity
		RETURN new`,
			{ body: body, id_creator: id_creator, id_super: id_super, time: Date.now() })
			.then((cursor) => {
				cursor.all().then((result) => res.status(200).send(result))
					.catch((error) => {
						console.log(`Router Entity - POST /entity failed: ${error}`);
						res.status(500).send(`Server Interval Error`);
					});
			})
			.catch((error) => {
				console.log(`Router Entities - POST /entity failed: ${error}`);
				res.status(500).send(`Server Interval Error`);
			});
	}
});

router.post('/connect', (req, res, next) => {
	let id_entity = `entity/${req.body.key_entity}`;
	let id_super = `entity/${req.body.key_super}`;
	let id_creator = `entity/${req.body.key_creator}`;
	let registrationId = req.body.registrationId;
	
	
//	const validate = Joi.validate(body, schemaEntity);
//	if (validate.error) {
//		res.status(400).send({ 'error': validate.error.toString(), 'model': { name: 'string', id: 'string', id_creatorEntity: 'string' } });
//	} else {

		db.query(`
		FOR e,h IN 1 ANY @id_entity hasEntitySuperEntity
		UPDATE h._key WITH {_from:@id_entity,_to:@id_super,registrationId:@registrationId} IN hasEntitySuperEntity

		INSERT {_type:'Connection',timeStart:@time, active:true} INTO event let new_event = NEW
		INSERT { _from:new_event._id, _to:@id_creator} INTO hasEventSender
		INSERT { _from:new_event._id, _to:'neutral/0'} INTO hasEventReceiver
		INSERT { _from:new_event._id, _to:@id_entity} INTO hasEventPurpose
		RETURN e`,
			{ registrationId:registrationId,id_creator: id_creator, id_entity: id_entity, id_super:id_super, time: Date.now() })
			.then((cursor) => {
				cursor.all().then((result) => res.status(200).send(result))
					.catch((error) => {
						console.log(`Router POST /entity/connect failed: ${error}`);
						res.status(500).send(`Router POST /entity/connect failed: ${error}`);
					});
			})
			.catch((error) => {
				console.log(`Router POST /entity/connect failed: ${error}`);
				res.status(500).send(`Router POST /entity/connect failed: ${error}`);
			});
//	}
});

router.post('/disconnect', (req, res, next) => {
	let id_entity = `entity/${req.body.key_entity}`;
	let id_creator = `entity/${req.body.key_creator}`;
	let id_super = `entity/-3` //Out

//	const validate = Joi.validate(body, schemaEntity);
//	if (validate.error) {
//		res.status(400).send({ 'error': validate.error.toString(), 'model': { name: 'string', id: 'string', id_creatorEntity: 'string' } });
//	} else {

		db.query(`
		FOR e,h IN 1 ANY @id_entity hasEntitySuperEntity
		UPDATE h._key WITH {_from:@id_entity,_to:@id_super} IN hasEntitySuperEntity

		INSERT {_type:'Connection',timeStart:@time, active:true} INTO event let new_event = NEW
		INSERT { _from:new_event._id, _to:@id_creator} INTO hasEventSender
		INSERT { _from:new_event._id, _to:'neutral/0'} INTO hasEventReceiver
		INSERT { _from:new_event._id, _to:@id_entity} INTO hasEventPurpose
		RETURN e`,
			{ id_creator: id_creator, id_entity: id_entity, id_super:id_super, time: Date.now() })
			.then((cursor) => {
				cursor.all().then((result) => res.status(200).send(result))
					.catch((error) => {
						console.log(`Router POST /entity/disconnect failed: ${error}`);
						res.status(500).send(`Router POST /disentity/connect failed: ${error}`);
					});
			})
			.catch((error) => {
				console.log(`Router POST /entity/disconnect failed: ${error}`);
				res.status(500).send(`Router POST /entity/disconnect failed: ${error}`);
			});
//	}
});


router.get('/administrative/:id/process/datail/pending', (req, res, next) => {
	let id_entity = `entity/${req.params.id}`;

	let query = `
	FOR ep IN 1 INBOUND @id_entity hasEventReceiver 
	FILTER ep._type == 'PromiseConnection' and 	ep.active==true  
	FOR p IN 1 OUTBOUND ep._id hasEventPurpose
	FOR s IN 1 ANY p._id hasProcessSubject
	FOR en,h IN 1 ANY p._id hasEntityCurrentProcess FILTER h.requester==true
		
	return {
	concerned:en,
	process:p,subjectProcess:s,
	event:ep
}
			`;
	db.query(query, { id_entity: id_entity })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
					res.status(500).send(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
			res.status(500).send(`Router - GET /administrative/:id//process/detail/pending failed: ${error}`);
		});
});

router.get('/:id/super', (req, res, next) => {
	let id_entity = `entity/${req.params.id}`;

	let query = `
	for en in 1 OUTBOUND @id_entity hasEntitySuperEntity
	return en
	`;
	db.query(query, { id_entity: id_entity })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET entity/:id/super failed: ${error}`);
					res.status(500).send(`Router - GET entity/:id/super failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET entity/:id/listBrother failed: ${error}`);
			res.status(500).send(`Router - GET entity/:id/super failed: ${error}`);
		});
});

router.delete('/:key', (req, res, next) => {

	let key = req.params.key;
	console.log('delete',key);
	
	db.query(
		`UPDATE @key WITH {status:'deleted'} IN entity let new_entity = NEW
		RETURN new_entity`,
		{key:key}
	).then((cursor) => {
		cursor.all().then((result) => res.status(200).send(result))
			.catch((error) => {
					console.log(`Router - DELETE /entity/${key} failed: ${error}`);
					res.status(500).send(`Router - DELETE /entity/${key} failed: ${error}`);
			});
		})
		.catch((error) => {
			console.log(`Router - DELETE /entity/${key} failed: ${error}`);
			res.status(500).send(`Router - DELETE /entity/${key} failed: ${error}`);
		});
})


router.get('/:key/canDelete', (req, res, next) => {
	//	let filters = filter.catchParams('entity', 'e', req.params);
	//	let query = `FOR e IN entity ${filters} RETURN e`;
	let id_entity = `entity/${req.params.key}`;
	
	let query = `let listProcess = (for p,h in 1 ANY @id_entity hasEntityCurrentProcess filter h.active == true return p)
	let listPromiseProcess = 
	(for evp in 1 ANY @id_entity hasEventReceiver filter evp._type=='PromiseConnection' 
		for p, h in 2 any evp._id hasEventPurpose, hasEntityCurrentProcess filter h.active == false return p)
	let listSubEntity = (for se in 1 inbound @id_entity hasEntitySuperEntity return se) 
	
	return {listProcess:listProcess,listPromiseProcess:listPromiseProcess,listSubEntity:listSubEntity}`;
	db.query(query, {id_entity:id_entity})
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET ${id_entity}/canDelete failed: ${error}`);
					res.status(500).send(`Router - GET ${id_entity}/canDelete failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET ${id_entity}/canDelete failed: ${error}`);
			res.status(500).send(`Router - GET ${id_entity}/canDelete failed: ${error}`);
		});
});

router.get('/levelDown/:id', (req, res, next) => {
	let idSuper = `entity/${req.params.id}`;
	
	let query = `
	for e in 1 INBOUND @idSuper  hasEntitySuperEntity  FILTER e.status!="deleted" return e`;
	db.query(query, { idSuper: idSuper })
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result)})
				.catch((error) => {
					console.log(`Router - GET entity/:id/levelDown failed: ${error}`);
					res.status(500).send(`Router - GET entity/:id/leveDown failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET entity/:id/listBrother failed: ${error}`);
			res.status(500).send(`Router - GET entity/:id/listBrother failed: ${error}`);
		});
});


router.get('/:id/listAdministrativeSibling/:key_subjectProcess', (req, res, next) => {
	let id_entity = `entity/${req.params.id}`;
	
	let id_subjectProcess = `subjectProcess/${req.params.key_subjectProcess}`; 
	
	
	let query = `
	let listEntitySubject = (for eenn in 1 ANY @id_subjectProcess canEntityConnectSubjectProcess
		return eenn._id)
	
	for en in 2..2 OUTBOUND @id_entity hasEntitySuperEntity,hasEntitySuperEntity
		for enn in 1 INBOUND en._id hasEntitySuperEntity FILTER enn._type =="Administrative" and enn._id NOT IN listEntitySubject AND enn.status!= 'deleted'
	return enn	`;
	db.query(query, { id_entity: id_entity,id_subjectProcess:id_subjectProcess })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET entity/:id/listBrother failed: ${error}`);
					res.status(500).send(`Router - GET entity/:id/listBrother failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET entity/:id/listBrother failed: ${error}`);
			res.status(500).send(`Router - GET entity/:id/listBrother failed: ${error}`);
		});
});

router.get('/:id/listSubject', (req, res, next) => {
	let id_entity = `entity/${req.params.id}`;

	let query = `
	FOR p,h IN 1 ANY @id_entity canEntityConnectSubjectProcess FILTER h.canCreateProcess == true OR h.canCreateProcess == 'true'
	return p`;
	db.query(query, { id_entity: id_entity })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET entity/:id/listSubject failed: ${error}`);
					res.status(500).send(`Router - GET entity/:id/listSubject failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET entity/:id/listSubject failed: ${error}`);
			res.status(500).send(`Router - GET entity/:id/listSubject failed: ${error}`);
		});
});

router.get('/id/:id', (req, res, next) => {
	let id = req.params.id;
	
	db.query(`FOR en IN entity FILTER en.id == @id 
	FOR add,h IN 1 ANY en._id hasEntityAddress
	return {
	_key:en._key,
	name:en.name,
	nameMom:en.nameMom,
	id:en.id,
	_type:en._type,
	email:en.email,
	phone:en.phone,
	address:{
			_key:add._key,
			logradouro:add.logradouro,
			cep:add.cep,
			bairro:add.bairro,
			localidade:add.localidade,
			complemento:h.complemento,
			firstId:h.firstId
			}
	}`, { id: id })
		//	db.collection('entity').firstExample({ id: id })
		.then((cursor) => {
			cursor.all().then((result) => {
				res.status(200).send(result)
			})
				.catch((error) => {
					console.log(`Router - GET entity/id/:id/ failed: ${error}`);
					res.status(500).send(`Router - GET entity/id/:id failed: ${error}`);
				})
		}).catch((error) => {
			console.log(`Router - GET entity/id/:id/ failed: ${error}`);
			res.status(500).send(`Router - GET entity/id/:id failed: ${error}`);
		});
});

/*
router.get('/:id/listSubjectDocument', (req, res) => {

	let id_entity = `entity/${req.params.id}`;
	let id_process = `process/${req.query.keyProcess}`

	db.query(`let listKeySubjectDocumentInProcess = (FOR sbd IN 2..2 ANY @id_process hasProcessDocument, hasDocumentSubject FILTER sbd._type == "Ordinary" return sbd._key )
	let id_subject = (FOR s IN 1 ANY @id_process hasProcessSubject return s._id)
	
	FOR en IN 1 ANY @id_entity hasEntitySuperEntity
		FOR sp,hsp IN ANY en._id canEntityConnectSubjectProcess FILTER sp._id == id_subject[0]
			FOR spsd IN ANY sp._id hasSubjectProcessSubjectDocument 
			FILTER POSITION(listKeySubjectDocumentInProcess,spsd._key) == false
			and LENGTH(INTERSECTION(hsp._listTypeDocumentCreation,[spsd._type]))>0
			
	return {subjectDocument:spsd,suggestionText:hsp.suggestionText}
	`, { id_entity: id_entity, id_process: id_process })
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
					res.status(500).send(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
				});
		}).catch((error) => {
			console.log(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
			res.status(500).send(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
		});
})
*/
router.get('/:id/listSubjectDocument', (req, res) => {

	let id_entity = `entity/${req.params.id}`;
	let id_process = `process/${req.query.keyProcess}`

	db.query(`

	let listSubCountAnn = (for e in 2 ANY @id_process hasProcessDocument, hasEventPurpose filter e._type=='Annulation' 
	for s in 2 OUTBOUND e._id hasEventPurpose, OUTBOUND hasDocumentSubject 
	collect ss=s with count into cc
	return {subjectDocument:ss,count:cc}
	)
	let listSubCountCre = (for e in 2 ANY @id_process hasProcessDocument, hasEventPurpose filter e._type=='Creation' 
	for s in 2 OUTBOUND e._id hasEventPurpose, OUTBOUND hasDocumentSubject 
	collect ss=s with count into cc
	return {subjectDocument:ss,count:cc}
	)
	
	let listNoShow = (
		for cre in listSubCountCre 
			for ann in listSubCountAnn filter cre.subjectDocument._key == ann.subjectDocument._key and 
			cre.count != ann.count
		return DISTINCT cre.subjectDocument
		)
			
	
	let listNoShow2 = (
		for cre in listSubCountCre filter not POSITION(listSubCountAnn,cre)
		return  cre.subjectDocument
		)
	
	
	let listSubDocSubProc = (
	for sd in 2 OUTBOUND @id_process hasProcessSubject, OUTBOUND hasSubjectProcessSubjectDocument filter sd._type =='Ordinary' 
	return sd)
	
	let listKeySubjectDocumentInProcess = REMOVE_VALUES(listSubDocSubProc,INTERSECTION(listSubDocSubProc,UNION(listNoShow,listNoShow2)))


let id_subject = (FOR s IN 1 ANY @id_process hasProcessSubject return s._id)

let listResultNotOrdinary=(FOR en IN 1 ANY @id_entity hasEntitySuperEntity
	FOR sp,hsp IN ANY en._id canEntityConnectSubjectProcess FILTER sp._id == id_subject[0]
		FOR spsd IN ANY sp._id hasSubjectProcessSubjectDocument FILTER 
//		POSITION(listKeySubjectDocumentInProcess,spsd) == false
//		and 
        spsd._type!= 'Ordinary' and
		LENGTH(INTERSECTION(hsp._listTypeDocumentCreation,[spsd._type]))>0
	return 	spsd)
	return union_distinct(listResultNotOrdinary, listKeySubjectDocumentInProcess)
					`, { id_process: id_process,id_entity:id_entity })
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					
					res.status(200).send(result.pop())
				})
				.catch((error) => {
					console.log(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
					res.status(500).send(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
				});
		}).catch((error) => {
			console.log(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
			res.status(500).send(`Router - GET process/:id/listSubjectDocument failed: ${error}`);
		});
})

router.get('/:id/from/process/to/listSubject', (req, res, next) => {
	let id_entity = `entity/${req.params.id}`;

	let query = `
	FOR p,h IN 1 OUTBOUND @id_entity hasEntityCurrentProcess FILTER h.active == true 
		FOR s IN 1 OUTBOUND p._id hasProcessSubject
		return DISTINCT s`;
	db.query(query, { id_entity: id_entity })
		.then((cursor) => {
			cursor.all()
				.then((result) => res.status(200).send(result))
				.catch((error) => {
					console.log(`Router - GET /:id/from/process/to/listSubject failed: ${error}`);
					res.status(500).send(`Router - GET /:id/from/process/to/listSubject failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /:id/from/process/to/listSubject failed: ${error}`);
			res.status(500).send(`Router - GET /:id/from/process/to/listSubject failed: ${error}`);
		});
});

router.get('/:id/listProcess', async (req, res, next) => {


	let id_entity = `entity/${req.params.id}`;

	let list_id_super = await db.query(`
	FOR super IN 1 OUTBOUND @id_entity hasEntitySuperEntity 
	FOR super_super IN 1 OUTBOUND super._id  hasEntitySuperEntity
	return {id_super:super._id,id_super_super:super_super._id} 	
	`,{id_entity:id_entity});

	let id_super_super = list_id_super._result[0].id_super_super;
	let id_super = list_id_super._result[0].id_super;

	resultProcess= await process_func.getFullDatail(
		id_process='',
		id_super_super,
		id_super,
		id_entity,
		'',
		'',
		'',
		'') 
	
	res.send(resultProcess)
});


router.get('/:id_e/subjectProcess/:id_s/listProcess', async(req, res, next) => {
	let id_entity = `entity/${req.params.id_e}`;
	let id_subject = `subjectProcess/${req.params.id_s}`;

	let list_id_super = await db.query(`
	FOR super IN 1 OUTBOUND @id_entity hasEntitySuperEntity 
	FOR super_super IN 1 OUTBOUND super._id  hasEntitySuperEntity
	FOR sub IN subjectProcess FILTER sub._id == @id_subject
	return {id_super:super._id,id_super_super:super_super._id,partNameSubject:sub.name} 	
	`,{id_entity:id_entity,id_subject:id_subject});

	let id_super_super = list_id_super._result[0].id_super_super;
	let id_super = list_id_super._result[0].id_super;
	let partNameSubject = list_id_super._result[0].partNameSubject;

	resultProcess= await process_func.getFullDatail(
		id_process='',
		id_super_super,
		id_super,
		id_entity,
		'',
		'',
		'',
		partNameSubject) 
	
	res.send(resultProcess)

});


router.get('/:id_super/listIndividualPersonIn', (req, res, next) => {

	let id_super = `entity/${req.params.id_super}`;
	let id_creator = `entity/${req.query.id_entity}`
	let query = `
	  FOR en IN 1 ANY @id_super hasEntitySuperEntity FILTER en._type == @_type and en._id != @id_entity 
	  return en`;
	db.query(query, { id_super: id_super, id_entity: id_creator, _type: 'IndividualPerson' })
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router Process - GET /process failed: ${error}`);
					res.status(500).send(`Server Interval Error`);
				});
		})
		.catch((error) => {
			console.log(`Router Process - GET /entity failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});


router.get('/:id/listConnectedProcess', (req, res, next) => {

	let id = `entity/${req.params.id}`;

	let query = `
	  FOR p, ep IN 1 ANY @id_entity hasEvent FILTER ep._type==@_type 
	  FOR s IN 1 ANY p._id hasProcessSubject    
			return {process:p,subject:s}`;
	db.query(query, { id_entity: id, _type: 'Concern' })
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router Process - GET /process failed: ${error}`);
					res.status(500).send(`Server Interval Error`);
				});
		})
		.catch((error) => {
			console.log(`Router Process - GET /entity failed: ${error}`);
			res.status(500).send(`Server Interval Error`);
		});
});


router.get('/:id/listIndividualPersonEntity', (req, res, next) => {
	let searchName = req.query.searchName;
	let id = `entity/${req.params.id}`;

	let query = `
	FOR s2 IN 1 INBOUND @id_entity hasEntitySuperEntity 
	FILTER 
	s2._type=='IndividualPerson' and 
	s2.status != 'deleted' 
	and (CONTAINS(LOWER(s2.name), LOWER(@searchName)) or @searchName == '')
	return s2`;
	db.query(query, { id_entity: id,searchName:searchName})
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router Entity - GET /${id}/listEntity failed: ${error}`);
					res.status(500).send(`Router Entity - GET /${id}/listEntity failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router Entity - GET /${id}/listEntity failed: ${error}`);
			res.status(500).send(`Router Entity - GET /${id}/listEntity failed: ${error}`);
		});
});

router.get('/:id/listAdministrativeEntity', (req, res, next) => {
	let searchName = req.query.searchName;
	let id = `entity/${req.params.id}`;

	let query = `
	FOR s2 IN 1 INBOUND @id_entity hasEntitySuperEntity 
	FILTER 
	s2._type=='Administrative' and 
	s2.status != 'deleted' 
	and (CONTAINS(LOWER(s2.name), LOWER(@searchName)) or @searchName == '')
	return s2`;
	db.query(query, { id_entity: id,searchName:searchName})
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router Entity - GET /${id}/listEntity failed: ${error}`);
					res.status(500).send(`Router Entity - GET /${id}/listEntity failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router Entity - GET /${id}/listEntity failed: ${error}`);
			res.status(500).send(`Router Entity - GET /${id}/listEntity failed: ${error}`);
		});
});

router.get('/:id/listEntityUser', (req, res, next) => {
	let id = `entity/${req.params.id}`;

	let query = `
	FOR en IN 1 INBOUND @id_entity hasEntitySuperEntity
	FILTER	en._type=='IndividualPerson' and en.status != 'deleted' 
	let user = (FOR u IN 1 ANY en._id hasEntityUser return u)
	
	return {entity:en,user:user[0]}`;
	db.query(query, { id_entity: id})
		.then((cursor) => {
			cursor.all()
				.then((result) => {
					res.status(200).send(result)
				})
				.catch((error) => {
					console.log(`Router Entity - GET /${id}/listEntityUser failed: ${error}`);
					res.status(500).send(`Router Entity - GET /${id}/listEntityUser failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router Entity - GET /${id}/listEntityUser failed: ${error}`);
			res.status(500).send(`Router Entity - GET /${id}/listEntityUser failed: ${error}`);
		});
});

router.get('/cityHall/listDownEntity', async (req, res) => {

	const id_cityHall = 'entity/0'; // prefeitura

	try {
		const result = await entity_db.getListEntityLevelDown(id_cityHall)
		
		res.status(200).send(result);
	} catch (error) {
		console.log(`Router - GET /cityHall/listDownEntity failed: ${error}`);
		res.status(500).send(error);
	}

});

router.get('/:id/canConnect_IN_AND_OUT/', (req, res, next) => {
	let id_administrative = `entity/${req.query.idSuper}`;
	let id_subject = `subjectProcess/${req.params.id}`;

	let query = `
	let superLoggin = (FOR super in  1 OUTBOUND @id_administrative hasEntitySuperEntity return super)
	
	let superExt =  (FOR e IN 1 ANY @id_subject canEntityConnectSubjectProcess 
		FOR super IN 1 OUTBOUND e._id hasEntitySuperEntity FILTER super._id != superLoggin[0]._id
		RETURN DISTINCT super)
		
	let levelDownExt = (FOR e in 1 INBOUND superExt[0]._id hasEntitySuperEntity filter e.status != "deleted" return e)
	
	
	let levelDownSub = (FOR e IN 1 ANY @id_subject canEntityConnectSubjectProcess filter e.status != "deleted" and e._id != @id_administrative 
		FOR super IN 1 OUTBOUND e._id hasEntitySuperEntity 
		RETURN e)
		
	let listInter = (for ee in levelDownSub filter not contains(levelDownExt,ee) return distinct ee )
	
	return APPEND(listInter,superExt)	
	`;
	db.query(query, { id_administrative: id_administrative, id_subject: id_subject })
		.then((cursor) => {
			cursor.all()
				.then((result) => { res.status(200).send(result[0]); })
				.catch((error) => {
					console.log(`Router GET entity/:id/canConnect_IN_AND_OUT/ failed: ${error}`);
					res.status(500).send(`Router - GET entity/:id/canConnect_IN_AND_OUT/ failed: ${error}`);
				});
		})
		.catch((error) => {
			console.log(`Router - GET /:id/canConnect/entity failed: ${error}`);
			res.status(500).send(`Router - GET /:id/canConnect/entity failed: ${error}`);
		});
});


module.exports = router;
