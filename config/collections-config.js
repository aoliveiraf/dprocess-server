const collectionConfig = [
    {name:"config", type: 1},
    {name:"entity", type: 1},
    {name:"user", type: 1},
    {name:"document", type: 1},
    {name:"process", type: 1},
    {name:"config",type: 1},
    {name:"subjectProcess",type:1},
    {name:"subjectDocument",type:1},
    {name:"taxable",type:1},
    {name:"event",type:1},// could be Creation, Connection, Rejection, PromiseConnection, Filing
    {name:"neutral",type:1},
    {name:"address",type:1},
    {name:"hasDocumentSubject",type:2},
    {name:"hasUserAdministratorEntity",type:2},
    {name:"hasEventSender",type:2},
    {name:"hasEventPurpose",type:2},
    {name:"hasEventReceiver",type:2},
    {name:"hasProcessSubject",type:2},
    {name:"hasEntityUser",type:2},
    {name:"hasEntitySuperEntity",type:2},
    {name:"hasProcessDocument",type:2},
    {name:"hasSubjectProcessSubjectDocument",type:2},
    {name:"canEntityConnectSubjectProcess",type:2}, // ConnectProcess
    {name:"hasEventPrevious",type:2},
    {name:"hasEntityCurrentProcess",type:2},
    {name:"hasEntityAddress",type:2},
    {name:"hasEntitySubjectProcess",type:2},
    {name:"hasEntityAdministrator",type:2}

];

module.exports = collectionConfig;