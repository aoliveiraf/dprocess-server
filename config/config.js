var systemVariables = {
    statusVariables: {
        ativo: 1,
        inativo: 2,
        cancelado: 3
    },
    rolesVariables: {
        manager: 1,
        operator: 2,
        client: 3
    }
};

module.exports = systemVariables;