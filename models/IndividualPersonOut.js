const joi = require('joi');

const schema = joi.object().keys({
	name: joi.string().required(),
	id:joi.string().required(), // cpf
	id_creator: joi.string().required(),
	id_super: joi.string().required(),
	id_address: joi.string().required(),
	email:joi.string().optional(),
	phone:joi.string().optional(),
	complemento:joi.string().optional(),
	firstId:joi.string().optional(),
	_type: joi.string().regex(/IndividualPersonOut/).required()
});
 
module.exports = schema;