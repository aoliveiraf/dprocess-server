const joi = require('joi');

const schema = joi.object().keys({
	name: joi.string().required(),
	nameMom: joi.string().required(),
	id:joi.string().required(), // cpf
	id_creator: joi.string().required(),
	id_super: joi.string().required(),
	_type: joi.string().regex(/IndividualPersonIn/).required()
});
 
module.exports = schema;