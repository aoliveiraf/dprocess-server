const joi = require('joi');

const schema = joi.object().keys({
	name: joi.string().required(),
	passwork: joi.string().required(),
	id_creator: joi.string().required(),
	id_entity: joi.string().required()
});

module.exports = schema;