var joi = require('joi');

const schema = joi.object().keys({
	id_subject: joi.string().required(),
    description: joi.string().allow(''),
    id:joi.string().required(), // código
	id_creator:joi.string().required(),
//	id_concernedIn:joi.string().required(),
    id_administrative: joi.string().required(),
    id_concernedOut: joi.string().required()
}); 

module.exports = schema;