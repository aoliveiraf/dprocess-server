var joi = require('joi');

const schema = joi.object().keys({
	time: joi.string().required(),
    _type:joi.string().regex('Creation'|'Connection'|'PromiseConnection'|'Rejection'|'Filing').required(),
	id_sender:joi.string().required(),
    id_receiver: joi.string(),
    id_previous: joi.string()
}); 

module.exports = schema;