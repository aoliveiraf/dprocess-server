"use strict";
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const fs = require('fs');
const pdf = require('html-pdf');
const crypto = require('crypto'); 




const utilFunction= {
	htmlToPDF: function (html,dir,fileName) {

//		let html = fs.readFileSync('./test/businesscard.html', 'utf8');
		let options = { format: 'Letter' };
 
		pdf.create(html, options).toFile(dir+'/'+fileName+'.pdf', function(err, res) {
  			if (err) return console.log(err);
  				console.log(res); // { filename: '/app/businesscard.pdf' }
		});
	},
	codeVerifier : function (data) {

		let sum =0;
		data=data.toString()
		for(let factor=2,i=data.length-1;i>=0;factor++,i--) {
			if(factor-1==9) {
				factor=2
			}
			sum+=parseInt(data[i])*factor;
		}
		let _result = ((sum*10)%11).toString();
		return _result[_result.length-1];
	}
	,
	sendEmail : function (data){// async..await is not allowed in global scope, must use a wrapper
		var transporter = nodemailer.createTransport(smtpTransport({
			service: 'gmail',
			tls: {
				rejectUnauthorized: false
			},
			auth: {
				   user: data.from,
				   pass: data.pass
			   }
		   }));		
		var mailOptions = {
			from: 'Não Responda ' + '<'+ data.from +'>',
			to: data.to,
			subject: data.process.subject,
			text: 'Seguem algumas informações sobre um processo aberto em ' + data.process.entity + '.' + '\n' + 
				'Número do Processo: ' + data.process.id + '\n' + 
				'Assunto: ' + data.process.subject + '\n' + 
				'Data: ' + data.process.time + '\n\n\n' +
				'Este email foi gerado automaticamente pelo sistema DigPross que é utilizado para o registro de processos.'

		}
		
		transporter.sendMail(mailOptions, function (err, res) {
			if(err){
				console.log('Error',err);
			} else {
				console.log('Email Sent');
			}
		})
		
	},
	user: {
		encryptPassword:function(password) {

			let saltAndHash = {
				
			}
			saltAndHash.salt = crypto.randomBytes(16).toString('hex'); 
			
			saltAndHash.hash = crypto.pbkdf2Sync(password, saltAndHash.salt,  
			1000, 64, `sha512`).toString(`hex`);
			
			return saltAndHash;
		},
		validPassword :function(password,saltAndHash) { 
			let hash = crypto.pbkdf2Sync(password,  
				saltAndHash.salt, 1000, 64, `sha512`).toString(`hex`);

			return hash === saltAndHash.hash; 
		}
	
	}
}
//let saltAndHash = utilFunction.user.encryptPassword('123');
//console.log(saltAndHash);

//let valid = utilFunction.user.validPassword('12',saltAndHash)

//console.log(valid);



//utilFunction.htmlToPDF('<h1>Teste htmlpdf</h1>')
//utilFunction.sendEmail({from:'noreplymossoro@gmail.com',pass:'N1r2ply3',to:'aoliveiraf@gmail.com',process:{id:'23323',entity:'SEFAZ',concerned:'Concerned',subject:'ITBI',time:'03/08/2019 10:30'}})
module.exports = utilFunction;




