const joi = require('joi');

const schema = joi.object().keys({
	name: joi.string().required(),
	id:joi.string().required(), // abreviation, code
	id_creator: joi.string().required(),
	id_super: joi.string().required(),
	_type: joi.string().regex(/Administrative/).required()
});
 
module.exports = schema;