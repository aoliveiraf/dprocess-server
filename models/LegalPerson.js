const joi = require('joi');

const schema = joi.object().keys({
	name: joi.string().required(),
	id:joi.string().required(), // cnpj
	id_creator: joi.string().required(),
	id_super: joi.string().required(),
	_type: joi.string().regex(/LegalPerson/).required()
});

module.exports = schema;