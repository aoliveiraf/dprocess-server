var joi = require('joi');

const schema = joi.object().keys({
	id_subject: joi.string().required(),
	id_creator:joi.string().required(),
	id_super:joi.string().required(),
	id_process:joi.string().required(),
	text:joi.string().allow('')
}); 

module.exports = schema;