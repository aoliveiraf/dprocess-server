const joi = require('joi');

const schema = joi.object().keys({
	name: joi.string().required(),
	id:joi.string().required(),
	id_creator: joi.string().required(),
	id_entity: joi.string().required(),
	_type:joi.string().regex(/Administrative|IndividualPerson|LegalPerson|ROOT|IN|OUT/).required()
});

module.exports = schema;